/*
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */

package geohunter;

import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

/** Responsible for providing an up-to-date location and compass direction */
@SuppressWarnings("deprecation")
public class GeoFixProviderLive implements LocationListener, SensorEventListener,
        GeoFixProvider {
    /** After this time duration, use the network location instead of old GPS value */
    public static final int GPS_TIMEOUT_SEC = 5*60;
    
    private GeoFix mLocation;
    private final LocationManager mLocationManager;
    private float mAzimuth;
    /** A refresh is sent whenever a sensor changes */
    private final ArrayList<Refresher> mObservers = new ArrayList<Refresher>();
    private final SensorManager mSensorManager;
    private boolean mUseNetwork = true;
    private final SharedPreferences mSharedPreferences;
    
    public GeoFixProviderLive(LocationManager locationManager,
            SensorManager sensorManager, SharedPreferences sharedPreferences) {
    	mLocationManager = locationManager;
        mSensorManager = sensorManager;
        mSharedPreferences = sharedPreferences;
        
        // mLocation = getLastKnownLocation(); //work in constructor..
    }

    public GeoFix getLocation() {
        if (mLocation == null) {
            Location lastKnownLocation = getLastKnownLocation();
            if (lastKnownLocation != null)
                mLocation = new GeoFix(lastKnownLocation);
            else
                mLocation = GeoFix.NO_FIX;
        }
        return mLocation;
    }

    public void addObserver(Refresher refresher) {
        if (!mObservers.contains(refresher))
            mObservers.add(refresher);
    }

    public void removeObserver(Refresher refresher) {
        mObservers.remove(refresher);
    }

    private void notifyObservers() {
        for (Refresher refresher : mObservers) {
            refresher.refresh();
        }
    }

    /**
     * Choose the better of two locations: If one location is newer and more
     * accurate, choose that. (This favors the gps). Otherwise, if one location
     * is newer, less accurate, but farther away than the sum of the two
     * accuracies, choose that. (This favors the network locator if you've
     * driven a distance and haven't been able to get a gps fix yet.)
     */
    private static GeoFix choose(GeoFix oldLocation, GeoFix newLocation) {
        if (oldLocation == null)
            return newLocation;
        if (newLocation == null)
            return oldLocation;

        long timeDiff = newLocation.getTime() - oldLocation.getTime();
        if (timeDiff > GPS_TIMEOUT_SEC*1000) {
            //Log.d("geohunter", "Discarding GeoFix that's " + timeDiff/60000.0 + " min older");
            return newLocation;
        }
        if (timeDiff >= -GPS_TIMEOUT_SEC*1000) {
            float distance = newLocation.distanceTo(oldLocation);
            // Log.d("geohunter", "onLocationChanged distance="+distance +
            // "  provider=" + newLocation.getProvider());
            if (distance < 1 && 
                    Math.abs(newLocation.getAccuracy() - oldLocation.getAccuracy()) < 1)
                return oldLocation;

             if (newLocation.getAccuracy() <= oldLocation.getAccuracy())
                 return newLocation;
             
             if (distance >= oldLocation.getAccuracy() + newLocation.getAccuracy()) {
                 return newLocation;
             }
        } else {
            Log.d("geohunter", "Got " + timeDiff / 1000 + "s older GeoFix from " +
                    newLocation.getProvider() + " than old GeoFix from " +
                    oldLocation.getProvider());
        }
        return oldLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null)
            return;
        GeoFix chosen = choose(mLocation, new GeoFix(location));
        if (chosen != mLocation) {
            mLocation = chosen;
            notifyObservers();
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("geohunter", "onProviderDisabled(" + provider + ")");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("geohunter", "onProviderEnabled(" + provider + ")");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        for (Refresher refresher : mObservers) {
            refresher.providerStateChanged(provider, status, extras);
        }
    }


    public void startUpdates() {
        Log.wtf("METER", "STARTING GEOFIX UPDATES");
        mUseNetwork = mSharedPreferences.getBoolean("use-network-location", true);
        if(Build.MODEL.equalsIgnoreCase("google_sdk") || Build.MODEL.equalsIgnoreCase("sdk"))
        	mUseNetwork = false;

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);

        long minTime = 1000; // ms
        float minDistance = 1; // meters

//        Criteria minCriteria = new Criteria();
//        // Disable unnecessary parts
//        minCriteria.setAltitudeRequired(false);
//        minCriteria.setBearingRequired(false); //TODO: check later if we want to use it
//        minCriteria.setCostAllowed(false);
//        // Request parts we need
//        minCriteria.setSpeedRequired(true);
//        minCriteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                minTime, minDistance, this);
        if (mUseNetwork)
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, minTime, minDistance,
                    this);

        onLocationChanged(getLastKnownLocation());
    }

    public void stopUpdates() {
        Log.wtf("METER", "STOPPING GEOFIX UPDATES");
        mSensorManager.unregisterListener(this);
        mLocationManager.removeUpdates(this);
    }

    public boolean isProviderEnabled() {
        return mLocationManager.isProviderEnabled("gps")
                || (mUseNetwork && mLocationManager.isProviderEnabled("network"));
    }

    public Location getLastKnownLocation() {
        Location gpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (gpsLocation != null)
            return gpsLocation;
        if (mUseNetwork)
            return mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        return null;
    }

    public float getAzimuth() {
        return mAzimuth;
    }

    // Added for below callbacks
    private long lastUpdate = -1;
    private float x, y, z;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 800;

    public boolean mIsMoving = false;
    public long mMovementTimestamp = 0;

    // from SensorEventListener
	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
		{ 
			long curTime = System.currentTimeMillis();
			// only allow one update every 100ms. 
			if ((curTime - lastUpdate) > 100) 
			{
				long diffTime = (curTime - lastUpdate); 
				lastUpdate = curTime; 
				x = event.values[0]; 
				y = event.values[1]; 
				z = event.values[2]; 
				float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
				if (speed > SHAKE_THRESHOLD) { 
					// yes, this is a shake action! Do something about it!
                    mIsMoving = true;
                    mMovementTimestamp = System.currentTimeMillis();
				}
				last_x = x; 
				last_y = y; 
				last_z = z;
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
    
}
