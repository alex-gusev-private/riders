package me.alexeygusev.riders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.JsonWriter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;

import crouton.Crouton;
import crouton.Style;
import me.alexeygusev.riders.pojo.Destination;
import me.alexeygusev.riders.pojo.OrderInfo;
import me.alexeygusev.riders.service.RidersGpsService;
import me.alexeygusev.riders.ws.ApiHelper;


public class DestinationActivity extends BaseActivity {

    public static final String TAG = "RIDERS-DEST";
    private Destination mDestination;
    private boolean mRequestInProgress = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        ApiHelper.getInstance().addListener(this);

        Bundle extras = savedInstanceState == null ? getIntent().getExtras() : savedInstanceState;
        mDestination = (Destination) extras.getSerializable("destination");
        final OrderInfo mOrderInfo = (OrderInfo) extras.getSerializable("order");
        mRequestInProgress = extras.getBoolean("request_in_progress", false);

        if (mDestination != null) {
            TextView textView;
            textView = (TextView) findViewById(R.id.tvDeliveryId);
            textView.setText(String.format("%s %d", getString(R.string.delivery_id), mDestination.mOrderPackageNumber));
            textView = (TextView) findViewById(R.id.tvCustomerName);
            if (mOrderInfo == null) {
                SuperToast superToast = new SuperToast(this);
                superToast.setAnimations(SuperToast.Animations.FADE);
                superToast.setDuration(SuperToast.Duration.SHORT);
                superToast.setBackground(SuperToast.Background.RED);
                superToast.setTextSize(SuperToast.TextSize.LARGE);
                superToast.setText(getString(R.string.err_no_info_found));
                superToast.show();
            } else {
                if (!TextUtils.isEmpty(mOrderInfo.mCustomerName))
                    textView.setText(String.format("%s %s", getString(R.string.customer_name), mOrderInfo.mCustomerName));
                textView = (TextView) findViewById(R.id.tvNote);
                if (!TextUtils.isEmpty(mOrderInfo.mCustomerNote))
                    textView.setText(mOrderInfo.mCustomerNote);
                textView = (TextView) findViewById(R.id.tvAddressInfo);
                if (!TextUtils.isEmpty(mOrderInfo.mAddress))
                    textView.setText(mOrderInfo.mAddress);
                textView = (TextView) findViewById(R.id.tvDeliveryInfo);
                if (!TextUtils.isEmpty(mOrderInfo.mCustomerOrderInfo))
                    textView.setText(mOrderInfo.mCustomerOrderInfo);
            }
            textView = (TextView) findViewById(R.id.tvAddressInfo);
            if (!TextUtils.isEmpty(mDestination.mAddress) &&
                (mOrderInfo == null || TextUtils.isEmpty(mOrderInfo.mAddress)))
                textView.setText(mDestination.mAddress);
            textView = (TextView) findViewById(R.id.tvDeliveryInfo);
            if (!TextUtils.isEmpty(mDestination.mMessage)  &&
                (mOrderInfo == null || TextUtils.isEmpty(mOrderInfo.mCustomerOrderInfo)))
                textView.setText(mDestination.mMessage);
        }

        final Button mDeliveryCompleted = (Button) findViewById(R.id.btnDeliveryCompleted);
        mDeliveryCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDeliveryCompleted();
            }
        });

        final Button mDeliveryNotCompleted = (Button) findViewById(R.id.btnDeliveryNotCompleted);
        mDeliveryNotCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDeliveryNotCompleted();
            }
        });

        final Button mSendCustomerLocation = (Button) findViewById(R.id.btnCustomerPosition);
        mSendCustomerLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RidersGpsService.mACCURACY > 30) {
                    SuperToast superToast = new SuperToast(DestinationActivity.this);
                    superToast.setAnimations(SuperToast.Animations.FADE);
                    superToast.setDuration(SuperToast.Duration.SHORT);
                    superToast.setBackground(SuperToast.Background.RED);
                    superToast.setTextSize(SuperToast.TextSize.LARGE);
                    superToast.setText(getString(R.string.invalid_accuracy));
                    superToast.show();
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(DestinationActivity.this);

                builder.setMessage(getString(R.string.should_send_customer_position));
                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendCustomerLocation();
                    }
                });

                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                });

                builder.create().show();

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApiHelper.getInstance().removeListener(this);
    }

    private void sendCustomerLocation() {
        try {
            double lat = RidersGpsService.mLAT != 0 ? RidersGpsService.mLAT : mDestination.mLatitude;
            double lon = RidersGpsService.mLON != 0 ? RidersGpsService.mLON : mDestination.mLongitude;
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("SetCustomerLocation")
                    .name("DataItem").beginObject()
                    .name("AddressId").value(mDestination.mAddressId)
                    .name("Latitude").value(lat)
                    .name("Longitude").value(lon)
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, "CUSTOMER LOCATION: " + request.toString());
            mRequestInProgress = true;
            ApiHelper.getInstance().fireSetCustomerInformationRequest(request);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            //Toast.makeText(DestinationActivity.this, "Request failed with error", Toast.LENGTH_SHORT).show();
            Crouton.showText(DestinationActivity.this, getString(R.string.request_failed_with_error), Style.ALERT);
            mRequestInProgress = false;
        }
    }

    private void sendDeliveryCompleted() {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("ConfirmOrderStatus")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("DestinationId").value(mDestination.mDestinationId)
                    .name("SalesOrderId").value(mDestination.mSalesOrderId)
                    .name("OrderId").value(mDestination.getOrderId())
                    .name("OrderStatus").value(ApiHelper.OrderStatus.Completed.ordinal())
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, "DELIVERY COMPLETED: " + request.toString());
            mRequestInProgress = true;
            mDestination.mOrderStatus = ApiHelper.OrderStatus.Completed.ordinal();
            ApiHelper.getInstance().fireSetDestinationRequest(request);
//            setResult(RESULT_OK);
//            finish();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            //Toast.makeText(DestinationActivity.this, "Request failed with error", Toast.LENGTH_SHORT).show();
            Crouton.showText(DestinationActivity.this, getString(R.string.request_failed_with_error), Style.ALERT);
            mRequestInProgress = false;
        }
    }

    private void sendDeliveryNotCompleted() {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("ConfirmOrderStatus")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("DestinationId").value(mDestination.mDestinationId)
                    .name("SalesOrderId").value(mDestination.mSalesOrderId)
                    .name("OrderId").value(mDestination.getOrderId())
                    .name("OrderStatus").value(ApiHelper.OrderStatus.NotCompleted.ordinal())
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, "DELIVERY NOT COMPLETED: " + request.toString());
            mRequestInProgress = true;
            mDestination.mOrderStatus = ApiHelper.OrderStatus.NotCompleted.ordinal();
            ApiHelper.getInstance().fireSetDestinationRequest(request);
//            setResult(RESULT_OK);
//            finish();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            //Toast.makeText(DestinationActivity.this, "Request failed with error", Toast.LENGTH_SHORT).show();
            Crouton.showText(DestinationActivity.this, getString(R.string.request_failed_with_error), Style.ALERT);
            mRequestInProgress = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_destination, menu);
        return false;//true;
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("destination", mDestination);
        outState.putBoolean("request_in_progress", mRequestInProgress);
    }

    @Override
    public void notifyError(String error) {
        if (mRequestInProgress) {
            Crouton.showText(this, error, Style.ALERT);
            mRequestInProgress = false;
        }
    }

    @Override
    public void notifySetDestinationFinished(JSONObject response) {
        if (mRequestInProgress) {
            Crouton.showText(DestinationActivity.this, getString(R.string.delivery_status_confirmed), Style.CONFIRM);
            Intent intent = new Intent(Constants.ACTION_DELIVERY_STATUS);
            intent.putExtra("destinationId", mDestination.mDestinationId);
            intent.putExtra("status", mDestination.mOrderStatus);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            mRequestInProgress = false;

            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void notifySetCarrierInformationFinished(JSONObject response) {
        if (mRequestInProgress) {
            Crouton.showText(DestinationActivity.this, getString(R.string.destination_confirmed), Style.CONFIRM);
            mRequestInProgress = false;
        }
    }
}
