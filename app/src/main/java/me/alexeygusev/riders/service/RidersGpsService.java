package me.alexeygusev.riders.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.JsonWriter;
import android.util.Log;
import android.widget.RemoteViews;

import com.github.johnpersano.supertoasts.SuperToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import me.alexeygusev.riders.BuildConfig;
import me.alexeygusev.riders.Constants;
import me.alexeygusev.riders.MapsActivity;
import me.alexeygusev.riders.R;
import me.alexeygusev.riders.RidersApp;
import me.alexeygusev.riders.db.DBAdapter;
import me.alexeygusev.riders.geofencing.GeofenceTransitionsIntentService;
import me.alexeygusev.riders.geofencing.SimpleGeofence;
import me.alexeygusev.riders.geofencing.SimpleGeofenceStore;
import me.alexeygusev.riders.pojo.Destination;
import me.alexeygusev.riders.pojo.GpsReport;
import me.alexeygusev.riders.runnable.RunOn;
import me.alexeygusev.riders.ws.ApiHelper;

public class RidersGpsService extends Service implements GpsReport.Callback, ApiHelper.ApiHelperListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final static String TAG = "RIDERS-SERVICE";

    Timer mBackupTimer;

    /** After this time duration, use the network location instead of old GPS value */
    //public static final int GPS_TIMEOUT_SEC = 60;
    private long mLastGpsFixPollTimestamp = 0;

    public static double mLAT, mLON, mACCURACY;
    public static boolean mIsMoving = false;
    public static int mActivityCode = DetectedActivity.UNKNOWN;
    public static ApiHelper.RiderStatus mRiderStatus = ApiHelper.RiderStatus.Available;

    private GoogleApiClient mGoogleApiClient;

    public static DBAdapter mDBAdapter;

    private GpsReport mPrevTask = null;
    private boolean mSendGPS = true;
    private static boolean mStatusRequestInProgress = false;

    // Constants that define the activity detection interval
    public static final int MSEC_PER_SECOND = 1000;
    public static final int DETECTION_INTERVAL_SECONDS = 20;
    public static final int DETECTION_INTERVAL_MSEC = MSEC_PER_SECOND * DETECTION_INTERVAL_SECONDS;

    /**
     * Store the PendingIntent used to send activity recognition events
     * back to the app
     */
    private PendingIntent mActivityRecognitionPendingIntent;

    // Internal List of Geofence objects. In a real app, these might be provided by an API based on
    // locations within the user's proximity.
    List<Geofence> mGeofenceList;
    // Persistent storage for geofences.
    private SimpleGeofenceStore mGeofenceStorage;
    // Stores the PendingIntent used to request geofence monitoring.
    private PendingIntent mGeofenceRequestIntent;

    private HashMap<String, String> mGeofenceDestinations = new HashMap<>();

    private Vibrator mVibratorService;

    @Override
    public void onConnected(Bundle bundle) {
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient,
                DETECTION_INTERVAL_MSEC, mActivityRecognitionPendingIntent).setResultCallback(
                new ResultCallback<Status>() {

                    @Override
                    public void onResult(Status arg0) {
                        if (arg0.isSuccess()) {
                            Log.d(TAG, "Updates Requested Successfully");
                        } else {
                            Log.d(TAG, "Updates could not be requested");
                        }
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (null != mGeofenceRequestIntent) {
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, mGeofenceRequestIntent);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public enum REQUEST_TYPE {START, STOP}
    private REQUEST_TYPE mRequestType = REQUEST_TYPE.START;

    public RidersGpsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mDBAdapter = new DBAdapter();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        mLAT = mLON = mACCURACY = 0;
                    }
                })
                .build();
        mGoogleApiClient.connect();

        ApiHelper.getInstance().addListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mActivityRecogniser,
                new IntentFilter(Constants.ACTION_RIDER_ACTIVITY));
        LocalBroadcastManager.getInstance(this).registerReceiver(mGeofenceReceiver,
                new IntentFilter(Constants.ACTION_GEO_CREATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mGeofenceTransitionReceiver,
                new IntentFilter(Constants.ACTION_GEO_TRANSITION));

        /**
         * Create the PendingIntent that Location Services uses
         * to send activity recognition updates back to this app.
         */
        Intent intent = new Intent(this, ActivityRecognitionIntentService.class);

        /**
         * Return a PendingIntent that starts the IntentService.
         */
        mActivityRecognitionPendingIntent =
                PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Instantiate a new geofence storage area.
        mGeofenceStorage = new SimpleGeofenceStore(this);
        // Instantiate the current List of geofences.
        mGeofenceList = new ArrayList<>();
        mGeofenceRequestIntent = getGeofenceTransitionPendingIntent();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mActivityRecogniser);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGeofenceReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGeofenceTransitionReceiver);

        super.onDestroy();

        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mGoogleApiClient, mActivityRecognitionPendingIntent);
        removeGeofences();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder<>(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //GpsAlarmReceiver.setRepeatingAlarm(RidersApp.getAppContext());
        GpsAlarmReceiver.cancelRepeatingAlarm(RidersApp.getAppContext());
        if (mBackupTimer != null) {
            mBackupTimer.cancel();
            mBackupTimer = null;
        }

        broadcastUnsentReports();

        Integer freq = intent != null ? intent.getExtras().getInt(Constants.TRANSMIT_FREQ, 10) : 10;

        mBackupTimer = new Timer("GPS-BACKUP");
        mBackupTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                pollLastKnownLocation();
            }
        }, 0, freq * 1000L);

        return START_STICKY;
    }

    private void pollLastKnownLocation() {
        Log.w(TAG, "POLL");

        if (!mGoogleApiClient.isConnected()) {
            Log.wtf(TAG, "LOCATION CLIENT IS NOT YET CONNECTED");
            return;
        }

        if (!mSendGPS) {
            Log.wtf(TAG, "SKIP GPS REPORTING");
            return;
        }

        if (!MapsActivity.mLoggedIn) {
            Log.wtf(TAG, "NOT LOGGED IN");
            return;
        }

        final Location theBest = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mACCURACY = theBest.getAccuracy();
        if (theBest == null || (theBest.hasAccuracy() && theBest.getAccuracy() > 50)) {
            executeNext();
            return;
        }
        RunOn.taskThread(new Runnable() {
            @Override
            public void run() {
                long currTime = System.currentTimeMillis();
                long delta = mLastGpsFixPollTimestamp > 0 ? currTime - theBest.getTime() : 0;
                if (delta < 0)
                    delta = 0;
                delta /= 1000;
                Log.wtf(TAG, "POLL: [" + delta + "] " + theBest.toString());
                String log = String.format(Locale.ENGLISH, "LOCATION: %s, SPEED: %f",
                        theBest.toString(), theBest.getSpeed());
                RidersGpsService.addLog(RidersApp.getAppContext(), "riders.txt", log);

                mDBAdapter.insertJob(theBest);
                executeNext();
                mLastGpsFixPollTimestamp = currTime;
                mLAT = theBest.getLatitude();
                mLON = theBest.getLongitude();
                mACCURACY = theBest.getAccuracy();
            }
        });
    }

    private boolean running;
    private int skipCount = 0;
    private int jobId = 0;

    private void executeNext() {
        if (running && skipCount < 3) {
            broadcastUnsentReports();
            skipCount++;
            Log.wtf(TAG, "TASK IS STILL RUNNING, EXITING...");
            return; // Only one task at a time.
        }

        skipCount = 0;

        broadcastUnsentReports();
        GpsReport task = mDBAdapter.getNextActiveJob();
//        GpsReportTask task = RidersApp.mGpsReportQueue.peek();
        if (task != null) {
            running = true;
            reportPosition(task);
        }
    }

    @Override
    public void onSuccess(int jobID) {
        Log.wtf(TAG, "REQUEST for GPS REPORT = [" + jobID + "] WAS SUCCESSFUL");
        running = false;
//        if (RidersApp.mGpsReportQueue.size() > 0)
//            RidersApp.mGpsReportQueue.remove();
        mDBAdapter.deleteJob(jobID);

        broadcastUnsentReports();

        executeNext();
    }

    private void broadcastUnsentReports() {
        int cnt = mDBAdapter.getActiveJobsCount();
        Intent intent = new Intent(Constants.ACTION_UNSENT_COUNT);
        intent.putExtra("count", cnt);
        LocalBroadcastManager.getInstance(this). sendBroadcast(intent);
    }

    @Override
    public void onFailure() {
        Log.wtf(TAG, "REQUEST FAILED");
        running = false;
    }

    public void reportPosition(GpsReport task) {
        jobId = task.mID;
        try {
            Log.wtf(TAG,
                    String.format(Locale.ENGLISH,
                            "REPORTING POSITION: (%.4f, %.5f)", task.mLat, task.mLon));
            if (task.mTimestamp < 1417392000) { // Dec 1st, 2014
                // Skip bad readings
                Log.wtf(TAG, "Skip bad timestamp: " + task.mTimestamp);
                notifySetCarrierInformationFinished(new JSONObject());
                return;
            }
            if (mPrevTask != null) {
                double distance = SphericalUtil.computeDistanceBetween(
                        new LatLng(task.mLat, task.mLon), new LatLng(mPrevTask.mLat, mPrevTask.mLon));
                //mDBAdapter.getLastMovement(task);
                String log = String.format(Locale.ENGLISH, "DIST: %.02f", distance);
                RidersGpsService.addLog(this, "riders.txt", log);
            }

            String createdOn = String.valueOf(task.mTimestamp);
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("SetCurrentPosition")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("Latitude").value(task.mLat)
                    .name("Longitude").value(task.mLon)
                    .name("Gps").value((int) task.mAccuracy)
                    .name("GPS").value((int) task.mAccuracy)
//                    .name("Gps").value(true)
//                    .name("InRange").value(isInRange)
                    .name("LogDate").value("/Date(" + createdOn + "-0000)/")
                    .name("StatusCode").value(mRiderStatus.ordinal())
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, request.toString());

            ApiHelper.getInstance().fireSetCarrierInformationRequest(request);

            mPrevTask = task;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void reportDriverStatus(ApiHelper.RiderStatus status) {
        try {
            Calendar cal = Calendar.getInstance();
            String createdOn = String.valueOf(cal.getTimeInMillis());

            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("SetCarrierStatus")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("LogDate").value("/Date(" + createdOn + "-0000)/")
                    .name("StatusCode").value(status.ordinal())
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, request.toString());

            mStatusRequestInProgress = true;
            mRiderStatus = status;
            ApiHelper.getInstance().fireSetCarrierInformationRequest(request);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            mStatusRequestInProgress = false;
        }
    }

    @Override
    public void notifyError(String error) {
        onFailure();
    }

    @Override
    public void notifyGetStaticValuesFinished(JSONObject response) {

    }

    @Override
    public void notifyGetDestinationFinished(JSONObject response) {

    }

    @Override
    public void notifySetDestinationFinished(JSONObject response) {

    }

    @Override
    public void notifySetCarrierInformationFinished(JSONObject response) {
        if (jobId > 0 && !mStatusRequestInProgress)
            onSuccess(jobId);
        else if (mStatusRequestInProgress) {
            mStatusRequestInProgress = false;
            running = false;
        } else
            running = false;
    }

    @Override
    public void notifySetCarrierInformationListFinished(JSONObject response) {

    }

    @Override
    public void notifySetCustomerInformationFinished(JSONObject response) {

    }

    @Override
    public void notifyGetOrderFinished(JSONObject response) {

    }

    @Override
    public void notifyLoginOperationsFinished(JSONObject response) {

    }

    @Override
    public void notifySetDeviceInformationFinished(JSONObject response) {

    }


    public static String getVersionString(Context ctx) {
        try {
            String versionName = "";
            PackageManager packageMgr = ctx.getPackageManager();
            if (packageMgr != null) {
                PackageInfo packageInfo = packageMgr.getPackageInfo(ctx.getPackageName(), 0);
                if (packageInfo != null)
                    versionName += "v" + packageInfo.versionName;
                else
                    versionName += "Unknown version";
            }
            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void addLog(Context ctx, String fileName, String text) {
        File f = new File(Environment.getExternalStorageDirectory(), fileName);
        if (!f.exists())
            try {
                f.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        try {
            if (f.exists()) {
                FileWriter fw = new FileWriter(f, true);
                fw.append(getVersionString(ctx)).append(" ")
                        .append(Calendar.getInstance().getTime().toGMTString()).append(": ")
                        .append(text).append("\r\n");
                fw.close();
                Log.wtf(TAG, text);
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    BroadcastReceiver mActivityRecogniser = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int activityType = intent.getIntExtra("activity_code", DetectedActivity.UNKNOWN);
            mActivityCode = activityType;
            Log.wtf(TAG, "SERVICE: Activity = " + activityType);
            //addLog(RidersApp.getAppContext(), "rider-activity.txt", "SERVICE: " + activityType);
            switch(activityType) {
                case DetectedActivity.IN_VEHICLE:
                case DetectedActivity.ON_BICYCLE:
                case DetectedActivity.ON_FOOT:
                case DetectedActivity.WALKING:
                case DetectedActivity.RUNNING:
                    RidersGpsService.mIsMoving = true;
                    break;
                case DetectedActivity.STILL:
                case DetectedActivity.UNKNOWN:
                case DetectedActivity.TILTING:
                    RidersGpsService.mIsMoving = false;
                default:
                    RidersGpsService.mIsMoving = false;
            }
        }
    };

    /**
     * Create a PendingIntent that triggers GeofenceTransitionIntentService when a geofence
     * transition occurs.
     */
    private PendingIntent getGeofenceTransitionPendingIntent() {
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    BroadcastReceiver mGeofenceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            removeGeofences();

            addLog(RidersApp.getAppContext(), "geo.txt", "CREATING GEOFENCES");
            ArrayList<Destination> destinations =
                    (ArrayList<Destination>) intent.getSerializableExtra("destinations");
            for (Destination dest: destinations) {
                SimpleGeofence geofence = new SimpleGeofence(
                        dest.mDestinationId,
                        dest.mLatitude,
                        dest.mLongitude,
                        MapsActivity.getDeviceId(RidersApp.getAppContext()).endsWith("1111") ? 150: 50,
                        24 * 60 * 60 * 1000, //24h
                        Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT
                );
                mGeofenceStorage.setGeofence(dest.mDestinationId, geofence);
                mGeofenceList.add(geofence.toGeofence());

                mGeofenceDestinations.put(dest.mDestinationId, dest.mAddress);
            }
            LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, mGeofenceList,
                    mGeofenceRequestIntent);
        }
    };

    BroadcastReceiver mGeofenceTransitionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String triggeredGeoFenceId = intent.getStringExtra("geofence_id");
            int transitionType = intent.getIntExtra("type", Geofence.GEOFENCE_TRANSITION_ENTER);
//            addLog(RidersApp.getAppContext(), "geo.txt", "GOT GEOFENCE: " + triggeredGeoFenceId + " TYPE = " + transitionType);
            String address = mGeofenceDestinations.get(triggeredGeoFenceId);
//            addLog(RidersApp.getAppContext(), "geo.txt", "ADDRESS ID: " + (!TextUtils.isEmpty(address) ? address : "NULL"));
            if (!TextUtils.isEmpty(address)) {
                if (transitionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    String content = getString(R.string.approaching_destination, address);
                    addNotification(getString(R.string.approaching_destination_title), address);
                    final SuperToast superToast = new SuperToast(RidersApp.getAppContext());
                    superToast.setAnimations(SuperToast.Animations.FADE);
                    superToast.setDuration(SuperToast.Duration.EXTRA_LONG);
                    superToast.setBackground(SuperToast.Background.BLUE);
                    superToast.setTextSize(SuperToast.TextSize.LARGE);
                    //superToast.setIcon(R.drawable.ic_launcher, SuperToast.IconPosition.LEFT);
                    superToast.setText(content);
                    superToast.show();
                    //Toast.makeText(RidersApp.getAppContext(), content, Toast.LENGTH_LONG).show();
                    if (!BuildConfig.DEBUG)
                        startVibration();
                    //playNotification();
                }
            }
        }
    };

    private void removeGeofences() {
        if (mGeofenceList.size() > 0) {
            List<String> ids = new ArrayList<>();
            for (Geofence g: mGeofenceList) {
                ids.add(g.getRequestId());
            }
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, ids);
        }
        mGeofenceList.clear();
        mGeofenceDestinations.clear();
    }

    public void startVibration() {
        long pattern[] = { 0, 500, 100, 500, 100, 500, 100, 500, 100, 500, 100, 500, 100, 500, 100 };
        mVibratorService = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mVibratorService.vibrate(pattern, -1);
    }

/*
    public void playNotification() {
        try {
            final MediaPlayer player;
            Uri notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setVolume(1,1);

            try {
                player.setDataSource(this, notificationUri);
                player.prepare();
            } catch (IllegalArgumentException | IllegalStateException | SecurityException | IOException e) {
                e.printStackTrace();
            }

            player.start();

            RunOn.taskThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                        player.stop();
                        player.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    void addNotification(String title, String content) {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, MapsActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        String packageName = getPackageName();
        RemoteViews remoteViews = new RemoteViews(packageName, R.layout.notification);
        //remoteViews.setTextViewText(R.id.tvTitle, title);
        remoteViews.setTextViewText(R.id.tvContent, content);
//        remoteViews.apply(this, null);

        Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
        bigTextStyle.bigText(content);
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.setSummaryText(title);
        long pattern[] = { 0, 500, 100, 500, 100 };//, 200, 100, 300, 100, 400, 100, 500, 100, 600 };
        Notification.Builder builder = new Notification.Builder(this);
        Notification notification = builder
//                .setStyle(bigTextStyle)
                .setContent(remoteViews)
                .setContentTitle(title)
                .setContentText(content)
//                .setTicker(content)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLights(0xFFFF0000, 2000, 100)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setVibrate(pattern)
                .setContentIntent(pendingIntent)
                .build();
        notification.bigContentView = remoteViews;

/*
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.ledARGB = 0xFFFF0000;
        notification.ledOnMS = 2000;
        notification.ledOnMS = 100;
*/
        //notification.flags |= Notification.FLAG_INSISTENT;// | Notification.FLAG_AUTO_CANCEL;
        //notification.defaults |= Notification.DEFAULT_LIGHTS | Notification.FLAG_ONLY_ALERT_ONCE;
        Random rand = new Random();
        int id = rand.nextInt();
        notificationManager.notify(id, notification);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wl.acquire();
    }

//    public Location getLastKownLocation() {
//        final Location theBest = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        mACCURACY = theBest.getAccuracy();
//        if (theBest == null || (theBest.hasAccuracy() && theBest.getAccuracy() > 50)) {
//            executeNext();
//            return;
//        }
//    }
}
