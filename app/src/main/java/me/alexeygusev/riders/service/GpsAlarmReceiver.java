package me.alexeygusev.riders.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class GpsAlarmReceiver extends BroadcastReceiver {

	public final static String ACTION_SETTINGS_ALARM = "me.alexeygusev.riders.SETTINGS_ALARM";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Bundle bundle = intent.getExtras();
			Log.wtf("TAG", "GOT BROADCAST");
		} catch (Exception e) {
			Toast.makeText(
                    context,
                    "There was an error somewhere, but we still received an alarm",
                    Toast.LENGTH_SHORT).show();
			e.printStackTrace();

		}
	}
	
	private static Intent getIntent(Context context, String id)
    {
            Intent i = new Intent(context, GpsAlarmReceiver.class);
            i.setAction(ACTION_SETTINGS_ALARM);
            i.setData(Uri.parse("timer:" + id));
            return i;
    }
	
	public static void setAlarm(Context context, String jobId, Calendar cal) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = getIntent(context, jobId);
        intent.putExtra("jobid", jobId);
        intent.putExtra("time", cal.getTimeInMillis());
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Date dt = cal.getTime();
        Date dtNow = Calendar.getInstance().getTime();
        long tm = cal.getTimeInMillis();
        Log.i("ALARM", String.format("SET: cal = " + DateFormat.format("yyyy-MM-dd HH:mm:ss", dt) +
                ", now = " + (String) DateFormat.format("yyyy-MM-dd HH:mm:ss", dtNow)));
        am.set(AlarmManager.RTC_WAKEUP, tm, sender);
	}

    public static void cancelAlarm(Context context, String jobId) {
        Intent intent = getIntent(context, jobId);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public static void setRepeatingAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent serviceIntent = new Intent(context, RidersGpsService.class);
        PendingIntent pi = PendingIntent.getService(context, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstTime = SystemClock.elapsedRealtime();
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 30*1000L, pi);
	}
	
	public static void cancelRepeatingAlarm(Context context) {
        Intent serviceIntent = new Intent(context, RidersGpsService.class);
        PendingIntent sender = PendingIntent.getService(context, 0, serviceIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
	}
}