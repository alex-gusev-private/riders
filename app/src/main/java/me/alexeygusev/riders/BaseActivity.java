package me.alexeygusev.riders;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.github.johnpersano.supertoasts.SuperToast;

import org.json.JSONObject;

import me.alexeygusev.riders.ws.ApiHelper;

public class BaseActivity extends FragmentActivity implements ApiHelper.ApiHelperListener {

	private static final String TAG = "RIDERS-BASE";

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ApiHelper.getInstance().addListener(this);
	}


    @Override
	protected void onDestroy() {
		super.onDestroy();
        ApiHelper.getInstance().removeListener(this);
	}

    @Override
    public void notifyError(String error) {
        SuperToast superToast = new SuperToast(this);
        superToast.setAnimations(SuperToast.Animations.FADE);
        superToast.setDuration(SuperToast.Duration.MEDIUM);
        superToast.setBackground(SuperToast.Background.RED);
        superToast.setTextSize(SuperToast.TextSize.LARGE);
        superToast.setText(getString(R.string.request_failed_with_error));
        superToast.show();
    }

    @Override
    public void notifyGetStaticValuesFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifyGetDestinationFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifySetDestinationFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifySetCarrierInformationFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifySetCarrierInformationListFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifySetCustomerInformationFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifyGetOrderFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }

    @Override
    public void notifyLoginOperationsFinished(JSONObject response) {
        Log.wtf("LOGIN", response.toString());
    }

    @Override
    public void notifySetDeviceInformationFinished(JSONObject response) {
        Log.wtf(TAG, response.toString());
    }
}
