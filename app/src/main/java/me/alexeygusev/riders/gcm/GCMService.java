package me.alexeygusev.riders.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;


public class GCMService extends Service {

    public static final String TAG = "GCM-SERVICE";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, String.format("SERVICE.onCreate(): PID = 0x%08X, TID = 0x%08X", Process.myPid(), Process.myTid()));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, String.format("SERVICE.onStartCommand(): PID = 0x%08X, TID = 0x%08X", Process.myPid(), Process.myTid()));
        return START_STICKY;
    }

    @SuppressWarnings("deprecation")
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "SERVICE.onDestroy()");
    }
}
