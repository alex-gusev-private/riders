package me.alexeygusev.riders.gcm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.github.johnpersano.supertoasts.SuperToast;

import java.util.Locale;
import java.util.Random;
import java.util.Set;

import me.alexeygusev.riders.MapsActivity;
import me.alexeygusev.riders.R;
import me.alexeygusev.riders.RidersApp;

/**
 * Created by Alex on 07/01/2015.
 */
public class GcmMessageBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("GCM", "GCM MESSAGE RECEIVED");
        Bundle extras = intent != null ? intent.getExtras() : null;
        if (extras == null)
            return;

        String body = "";
        Log.d("GSM", "==========> INTENT EXTRAS DUMP");
        Set<String> keys = extras.keySet();
        if (keys != null) {
            for (String key: keys)
                Log.wtf(key,String.valueOf(extras.get(key)));

            body = extras.getString("message");
            final SuperToast superToast = new SuperToast(context);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.EXTRA_LONG);
            superToast.setBackground(SuperToast.Background.GREEN);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(body);
            superToast.show();
        }
        Log.d("GSM", "<========== INTENT EXTRAS DUMP");

        addNotification(context, context.getString(R.string.message_for_driver), body);

        setResultCode(Activity.RESULT_OK);
    }

    void addNotification(Context context, String title, String content) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(context, MapsActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        String packageName = context.getPackageName();
        RemoteViews remoteViews = new RemoteViews(packageName, R.layout.notification);
        remoteViews.setTextViewText(R.id.tvContent, content);

        Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
        bigTextStyle.bigText(content);
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.setSummaryText(title);
        long pattern[] = { 0, 500, 100, 500, 100 };
        Notification.Builder builder = new Notification.Builder(context);
        Notification notification = builder
                .setContent(remoteViews)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLights(0xFFFF0000, 2000, 100)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setVibrate(pattern)
                .setContentIntent(pendingIntent)
                .build();
        notification.bigContentView = remoteViews;

        Random rand = new Random();
        int id = rand.nextInt();
        notificationManager.notify(id, notification);

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wl.acquire();
    }
}
