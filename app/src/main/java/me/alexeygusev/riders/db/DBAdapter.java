package me.alexeygusev.riders.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

import me.alexeygusev.riders.RidersApp;
import me.alexeygusev.riders.pojo.DestinationReport;
import me.alexeygusev.riders.pojo.GpsReport;
import me.alexeygusev.riders.ws.ApiHelper;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
public class DBAdapter {

    public static final String TAG = "RIDERS-DBAdapter";
	public static final String JOBS_TABLE = "jobs";
    public static final String DESTINATIONS_TABLE = "destinations";

	private SQLiteDatabase database;
	private DBHelper dbHelper;

	public DBAdapter() {
		
	}

	public static final Semaphore databaselLockSemaphore = new Semaphore(1);

	public DBAdapter open() throws SQLException {
		
		try {
			databaselLockSemaphore.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		Log.i(TAG, "Acquired DB Semaphore");

		if(database == null || !database.isOpen()) {
			dbHelper = new DBHelper(RidersApp.getAppContext());
			database = dbHelper.getWritableDatabase();
		}
		return this;
	}
	
	public boolean tryOpen() throws SQLException {
		try {
			if (!databaselLockSemaphore.tryAcquire())
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		Log.i(TAG, "Acquired DB Semaphore");

		if(database == null || !database.isOpen()) {
			dbHelper = new DBHelper(RidersApp.getAppContext());
			database = dbHelper.getWritableDatabase();
			
		}

		return true;
	}

	public DBHelper getHelper()
	{
		return dbHelper;
	}

	public SQLiteDatabase getDB()
	{
		return database;
	}

	public Boolean isDBOpen() {
		try {
			return database.isOpen();
		} catch(Exception e) {
			Log.e(TAG, "ERROR : Checking DB state");
			return false;
		}
	}
	
	public void close() {
		try
		{
			dbHelper.close();
		}
		catch(Exception e)
		{
			Log.e(TAG, "ERROR : Closing DB");
		}
		databaselLockSemaphore.release();
		Log.i(TAG, "Released DB Semaphore");
	}
	
	private ContentValues createContentValues(Map<String, Object> params) {
		ContentValues values = new ContentValues();
		for(Entry<String, Object> entry : params.entrySet())
		{
			values.put(entry.getKey(), entry.getValue()+"");
		}
		return values;
	}

    public boolean insertJob(Location location) {
        if (location == null)
            return false;

        long timestamp = System.currentTimeMillis();
        if (timestamp < 1417392000)
            timestamp = location.getTime();
        ContentValues cv = new ContentValues();
        cv.put("lat", location.getLatitude());
        cv.put("lon", location.getLongitude());
		cv.put("accuracy", location.getAccuracy());
        cv.put("gps", 1);
        cv.put("timestamp", timestamp);
        cv.put("sent", 0);
        return insertJob(cv);
    }

	public boolean insertJob(ContentValues params) {
		try {
			open();
			long idForNewRow = database.replace(JOBS_TABLE, null, params);
			if (idForNewRow < 0) {
				Log.e(TAG, "Failed to insert job");
				return false;
			}
			
			return true;
			
		} catch(Exception e) {
			Log.e(TAG, "ERROR : Inserting CurrentJob");
			e.printStackTrace();
			return false;
		} finally {
			close();
		}
	}
	
	public ArrayList<GpsReport> getAllActiveJobs() {
		Cursor cr = null;
		try {
			open();
			cr =  database.query(JOBS_TABLE, null, "sent = 0", null, null,	null, null);
			
			ArrayList<GpsReport> list = new ArrayList<>();
						
			while (cr.moveToNext()) {
                GpsReport currentJob = getJobFromCursor(cr);
				list.add(currentJob);
			}
			
			return list;
		} catch(Exception e) {
			Log.e(TAG, "ERROR : Getting All Active Jobs");
			e.printStackTrace();
			return null;
		} finally {
			if(cr != null && !cr.isClosed()) {
				cr.close();
			}
			close();
		}
	}

    public GpsReport getNextActiveJob() {
        Cursor cr = null;
        try {
            open();
            cr =  database.query(JOBS_TABLE, null, "sent = 0", null, null,	null, "_id ASC");

            if (cr.moveToNext()) {
                return getJobFromCursor(cr);
            }
        } catch(Exception e) {
            Log.e(TAG, "ERROR : Getting Next Active Job");
            e.printStackTrace();
            return null;
        } finally {
            if(cr != null && !cr.isClosed()) {
                cr.close();
            }
            close();
        }
        return null;
    }
	
	public int getActiveJobsCount() {
		Cursor cr = null;
		try {
			open();
			cr =  database.rawQuery("select count(_id) from jobs WHERE sent = 0", null);
			if (cr.moveToNext()) {
				Integer count = cr.getInt(0);
				return count;
			} else
				return 0;
		} catch(Exception e) {
            Log.e(TAG, "ERROR : Getting CurrentJob count");
            e.printStackTrace();
            return 0;
        } finally {
			if(cr != null && !cr.isClosed()) {
				cr.close();
			}
			close();
		}
	}

	public boolean deleteJobs() {
		try {
			open();
			return database.delete(JOBS_TABLE, null, null) > 0;
		} catch(Exception e) {
			Log.e(TAG, "ERROR : Deleting ALL Jobs");
			e.printStackTrace();
			return false;
		} finally {
			close();
		}
	}

	public boolean deleteJob(int jobId) {
		try {
			open();
			return database.delete(JOBS_TABLE, "_id = " + jobId, null) > 0;
		} catch(Exception e) {
			Log.e(TAG, "ERROR : Deleting Job");
			e.printStackTrace();
			return false;
		} finally {
			close();
		}
	}

	private GpsReport getJobFromCursor(Cursor cr) {
        GpsReport currentJob = new GpsReport();
		
		currentJob.mID = cr.getInt(cr.getColumnIndex("_id"));
		currentJob.mLat = cr.getDouble(cr.getColumnIndex("lat"));
        currentJob.mLon = cr.getDouble(cr.getColumnIndex("lon"));
		currentJob.mAccuracy = cr.getDouble(cr.getColumnIndex("accuracy"));
		currentJob.mGps = cr.getInt(cr.getColumnIndex("gps")) > 0;
		currentJob.mTimestamp = cr.getLong(cr.getColumnIndex("timestamp"));
		currentJob.mSent = cr.getInt(cr.getColumnIndex("sent")) > 0;

		return currentJob;
	}

/*
    public double getLastMovement(GpsReport job) {
        double distance = 0;

        Cursor cr = null;
        try {
            open();
            cr =  database.rawQuery("select lat,lon from jobs order by _id desc", null);
            if (cr.moveToFirst()) {
                GpsReport prev = getJobFromCursor(cr);
                distance = SphericalUtil.computeDistanceBetween(
                        new LatLng(job.mLat, job.mLon), new LatLng(prev.mLat, prev.mLon));
            }
        } catch(Exception e) {
            Log.e(TAG, "ERROR : getLastMovement()");
            e.printStackTrace();
            return -1;
        } finally {
            if(cr != null && !cr.isClosed()) {
                cr.close();
            }
            close();
        }

        return distance;
    }
*/

    public boolean insertDestination(ContentValues params) {
        try {
            open();
            long idForNewRow = database.replace(DESTINATIONS_TABLE, null, params);
            if (idForNewRow < 0) {
                Log.e(TAG, "Failed to insert destination");
                return false;
            }

            return true;

        } catch(Exception e) {
            Log.e(TAG, "ERROR : Inserting destination");
            e.printStackTrace();
            return false;
        } finally {
            close();
        }
    }

    public ArrayList<DestinationReport> getRecentActiveDestinations() {
        Cursor cr = null;
        try {
            open();
            cr =  database.query(JOBS_TABLE, null, "sent = 0", null, null,	null, null);

            ArrayList<DestinationReport> list = new ArrayList<>();

            while (cr.moveToNext()) {
                DestinationReport currentDest = getDestinationFromCursor(cr);
                list.add(currentDest);
            }

            return list;
        } catch(Exception e) {
            Log.e(TAG, "ERROR : Getting All Active Jobs");
            e.printStackTrace();
            return null;
        } finally {
            if(cr != null && !cr.isClosed()) {
                cr.close();
            }
            close();
        }
    }

    private DestinationReport getDestinationFromCursor(Cursor cr) {
        DestinationReport currentDest = new DestinationReport();

        currentDest.mAddressId = cr.getString(cr.getColumnIndex("address_id"));
        currentDest.mDestinationId = cr.getString(cr.getColumnIndex("destination_id"));
        currentDest.mOrderId = cr.getString(cr.getColumnIndex("order_id"));
        currentDest.mStatus = ApiHelper.OrderStatus.values()[cr.getInt(cr.getColumnIndex("status"))];
        currentDest.mTimestamp = cr.getLong(cr.getColumnIndex("timestamp"));
        currentDest.mSent = cr.getInt(cr.getColumnIndex("sent")) > 0;

        return currentDest;
    }
}
