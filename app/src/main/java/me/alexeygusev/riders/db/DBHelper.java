package me.alexeygusev.riders.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	
	
	private static final String DATABASE_NAME = "riders.db";

	private static final int DATABASE_VERSION = 3;

	private static final String JOBS = "create table if not exists jobs (" +
			"_id integer primary key autoincrement, " +
			"lat real not null, " +
			"lon real not null, " +
			"accuracy real not null default 0.0, " +
			"gps integer not null default 1," +
			"timestamp integer not null," +
            "sent integer not null default 0);";
    private static final String DESTINATIONS = "create table if not exists destinations (" +
            "_id integer primary key autoincrement, " +
            "address_id not null text default ''," +
            "destination_id not null text default ''," +
            "order_id text not null default ''," +
            "status integer not null default 0," +
            "timestamp integer not null," +
            "sent integer not null default 0);";

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(JOBS);
            db.execSQL(DESTINATIONS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS jobs");
        db.execSQL("DROP TABLE IF EXISTS destinations");
		onCreate(db);
	}
}
