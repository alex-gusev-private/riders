package me.alexeygusev.riders;

import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.JsonWriter;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.util.regex.Matcher;

import me.alexeygusev.riders.ws.ApiHelper;

public class ForgotPasswordActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = "RIDERS-LOGIN";

	Button btnOK;
	Button btnCancel;
	EditText etFullName;
	EditText etPhone;
    EditText etEmail;

	int mVersionCode = 0;
    ProgressDialog mProgress;

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_forgot_password);

		TextView version = (TextView) findViewById(R.id.textViewVersion);
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo("me.alexeygusev.riders", 0);
			version.setText(getString(R.string.app_name) + " v" + info.versionName);
		} catch (NameNotFoundException e) {
			
			e.printStackTrace();
		}
		if (info != null)
			mVersionCode = info.versionCode;

        etFullName = (EditText) findViewById(R.id.editTextFullName);
        etPhone = (EditText) findViewById(R.id.editTextPhone);
        etEmail = (EditText) findViewById(R.id.editTextEmail);

        etFullName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                etPhone.requestFocus();
                return true;
            }
        });
        etPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                etEmail.requestFocus();
                return true;
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                handleOkClick();
                return true;
            }
        });

		btnOK = (Button) findViewById(R.id.buttonOK);
		btnOK.setOnClickListener(this);
		
		btnCancel = (Button) findViewById(R.id.buttonCancel);
		btnCancel.setOnClickListener(this);

		Log.i(TAG, this.getClass().getSimpleName() + ".onCreate()");

        etFullName.requestFocus();
	}

	@Override
	public void onClick(View v) {
		if (v == btnOK) {
            handleOkClick();
		} else if (v == btnCancel) {
			setResult(RESULT_CANCELED);
			finish();
		}
	}

    private void handleOkClick() {
        String fullName = etFullName.getEditableText().toString();
        String phone = etPhone.getEditableText().toString();
        String email = etEmail.getEditableText().toString();
        if (TextUtils.isEmpty(fullName) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(email)) {
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.invalid_data));
            superToast.show();
            return;
        }

        if (!validEmailPattern(email)) {
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.invalid_email));
            superToast.show();
            etEmail.requestFocus();
            return;
        }

        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.talking_to_server));
        mProgress.setTitle(R.string.app_name);
        mProgress.setIcon(R.drawable.ic_launcher);
        mProgress.show();

        try {
            ApiHelper.getInstance().addListener(this);

            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject()
                    .name("request").beginObject()
                    .name("LoadOptions").value("ForgetPassword")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("EmailAddress").value(email)
                    .name("PhoneNumber").value(phone)
                    .name("NameAndLastName").value(fullName)
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            ApiHelper.getInstance().fireLoginOperationsRequest(request);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            ApiHelper.getInstance().removeListener(this);
            if (mProgress != null && mProgress.isShowing()) {
                mProgress.dismiss();
                mProgress = null;
            }
        }

        setResult(RESULT_OK);
        finish();
    }

    /**
     * Validates email for correctness
     * @param emailAddress Entered email address to be validated
     */
    private boolean validEmailPattern(String emailAddress) {
        if (TextUtils.isEmpty(emailAddress))
            return false;
        /**
         * For email format rules see {@link http://tools.ietf.org/html/rfc3696}
         */
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(emailAddress);
        return matcher.matches();
    }

    @Override
    public void notifyError(String error) {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }

        super.notifyError(error);
    }

    @Override
    public void notifyLoginOperationsFinished(JSONObject response) {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }

        Log.wtf("FORGOT", response.toString());
        try {
            JSONObject obj = new JSONObject(response.toString());
            String errorCode = obj.getString("ErrorCode");
            if (TextUtils.isEmpty(errorCode) || errorCode.equalsIgnoreCase("null")) {
                setResult(RESULT_OK);
                finish();
            } else {
                SuperToast superToast = new SuperToast(this);
                superToast.setAnimations(SuperToast.Animations.FADE);
                superToast.setDuration(SuperToast.Duration.LONG);
                superToast.setBackground(SuperToast.Background.RED);
                superToast.setTextSize(SuperToast.TextSize.LARGE);
                superToast.setText(getString(R.string.request_failed_with_error));
                superToast.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.request_failed_with_error));
            superToast.show();
        }
    }
}
