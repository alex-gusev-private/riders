package me.alexeygusev.riders;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.JsonWriter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import crouton.Crouton;
import crouton.Style;
import me.alexeygusev.riders.pojo.Destination;
import me.alexeygusev.riders.pojo.GetDestinationResult;
import me.alexeygusev.riders.pojo.GetStaticValuesResult;
import me.alexeygusev.riders.pojo.OrderInfo;
import me.alexeygusev.riders.pojo.StaticValues;
import me.alexeygusev.riders.runnable.RunOn;
import me.alexeygusev.riders.service.GpsAlarmReceiver;
import me.alexeygusev.riders.service.RidersGpsService;
import me.alexeygusev.riders.ws.ApiHelper;

public class MapsActivity extends BaseActivity {

    /**
     * The empty string to return if an ID can't be found
     */
    public static final String NO_ID_EMPTY_STRING = "";
    public static final String TAG = "RIDERS";

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Location mLocation;
    private int mScreenHeight;

    private RelativeLayout mPopupMenuPanel;
    private Button mToggleMessages;
    private TextView mGpsInfo, mUnsentInfo, mActivityDesc, mStatusDesc;//, mAccuracyDesc;

    private ArrayList<String> mMockupOptionsArray = new ArrayList<>();

    private Timer mGpsInfoTimer = new Timer();

    private HashMap<Marker, Destination> mMarkersMap = new HashMap<>();
    private HashMap<String, OrderInfo> mOrdersMap = new HashMap<>();

    private StaticValues mAppSettings = new StaticValues();
    public static boolean mLoggedIn = false;
    private boolean mTrackMyPosition = true;

    /**
     * GCM-related members
     */
    GoogleCloudMessaging mGCM;
    AtomicInteger mMsgId = new AtomicInteger();
    String mRegId;

    //public static String mLastLoadOptions = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (!servicesConnected()) {
            //Toast.makeText(this, "FATAL:Google Play Services aren't available! ", Toast.LENGTH_SHORT).show();
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText("FATAL:Google Play Services aren't available!");
            superToast.show();
            finish();
            return;
        }

        setUpMapIfNeeded();

        applySettings(false);
        //ApiHelper.getInstance().addListener(this);

        mPopupMenuPanel = (RelativeLayout) findViewById(R.id.rlPopupMenuPanel);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        mScreenHeight = dm.heightPixels;
        mPopupMenuPanel.setY(-mScreenHeight);

        final ListView mMenuOptions = (ListView) findViewById(R.id.lvOptions);
        mMockupOptionsArray.add(getString(R.string.status_resting));
        mMockupOptionsArray.add(getString(R.string.status_accident));
        mMockupOptionsArray.add(getString(R.string.status_broken_down));
        mMockupOptionsArray.add(getString(R.string.status_waiting_customer));
        mMockupOptionsArray.add(getString(R.string.status_parking));
        mMockupOptionsArray.add(getString(R.string.get_next_delivery));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.messages_item_text,
                android.R.id.text1, mMockupOptionsArray);
        mMenuOptions.setAdapter(adapter);
        mMenuOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mToggleMessages.performClick();
                        RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Resting);
                        checkDeliveriesStatus();
                        break;
                    case 1:
                        mToggleMessages.performClick();
                        RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Accident);
                        break;
                    case 2:
                        mToggleMessages.performClick();
                        RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.BrokeDown);
                        break;
                    case 3:
                        mToggleMessages.performClick();
                        RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.WaitingCustomer);
                        break;
                    case 4:
                        mToggleMessages.performClick();
                        RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Parking);
                        break;
                    case 5:
                        mToggleMessages.performClick();
                        getNextDestination();
                        break;
                    default:
                        break;
                }
            }
        });

        mGpsInfo = (TextView) findViewById(R.id.tvGPSInfo);
        mUnsentInfo = (TextView) findViewById(R.id.tvUnsentInfo);
        mActivityDesc = (TextView) findViewById(R.id.tvActivity);
        mStatusDesc = (TextView) findViewById(R.id.tvStatus);
        //mAccuracyDesc = (TextView) findViewById(R.id.tvAccuracy);

        final Button getDestinations = (Button) findViewById(R.id.btnGetDestinations);
        getDestinations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDestination();
            }
        });

        mToggleMessages = (Button) findViewById(R.id.btnToggleMessages);
        mToggleMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int delta = 0;
                if (mPopupMenuPanel.getY() < 0)
                    delta = mScreenHeight;
                else
                    delta = -mScreenHeight;
                mPopupMenuPanel.animate().yBy(delta).setDuration(1000);
                //getCarrierStatusFromServer();
            }
        });

        final Button logout = (Button) findViewById(R.id.btnLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoggedIn = false;
                mPopupMenuPanel.animate().yBy(-mScreenHeight).setDuration(1000);
                //startActivityForResult(new Intent(MapsActivity.this, LoginActivity.class), Constants.LOGIN_REQUEST_CODE);

                // Perform harakiri...
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
            }
        });

        final ImageButton mTrackMe = (ImageButton) findViewById(R.id.btnTrackMe);
        mTrackMe.setBackgroundResource(mTrackMyPosition ? R.drawable.rectangle_white_50 : R.drawable.rectangle_white_50_redborder);
        mTrackMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMapCentering();
            }
        });

        mGpsInfoTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mMap == null)
                    return;
                RunOn.mainThread(new Runnable() {
                    @Override
                    public void run() {
                        Location loc = mMap.getMyLocation();
                        if (loc != null) {
                            String locStr = String.format(Locale.ENGLISH, "[%.4f, %.4f, %.0fm]", loc.getLatitude(), loc.getLongitude(), loc.getAccuracy());
                            mGpsInfo.setText(locStr);

                            centerMap(loc);
                        }
                        String activityDesc = getNameFromType(RidersGpsService.mActivityCode);
                        mActivityDesc.setText(activityDesc);
                        mStatusDesc.setText(RidersGpsService.mRiderStatus.toString());
                        //mAccuracyDesc.setText(String.format(Locale.ENGLISH, "ACC: %.0fm", RidersGpsService.mACCURACY));
                    }
                });
            }
        }, 1000, 1000);

        LocalBroadcastManager.getInstance(this).registerReceiver(mUnsentCountReceiver, new IntentFilter(Constants.ACTION_UNSENT_COUNT));
        LocalBroadcastManager.getInstance(this).registerReceiver(mSettingsUpdateReceiver, new IntentFilter(GpsAlarmReceiver.ACTION_SETTINGS_ALARM));
        LocalBroadcastManager.getInstance(this).registerReceiver(mDeliveryStatusReceiver, new IntentFilter(Constants.ACTION_DELIVERY_STATUS));
        startGpsService();

        updateSettingsFromServer();

        if (savedInstanceState == null) {
            mLoggedIn = false;
            startActivityForResult(new Intent(this, LoginActivity.class), Constants.LOGIN_REQUEST_CODE);
        } else {
            mLoggedIn = savedInstanceState.getBoolean("loggedIn", false);
            if (!mLoggedIn) {
                startActivityForResult(new Intent(this, LoginActivity.class), Constants.LOGIN_REQUEST_CODE);
            }
        }
//        if (mLoggedIn)
//            RunOn.mainThreadDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getDestination();
//                }
//            }, 500);

        mGCM = GoogleCloudMessaging.getInstance(this);
        mRegId = getRegistrationId(this.getApplicationContext());

        if (TextUtils.isEmpty(mRegId)) {
            registerInBackground();
        } else {
            Log.i(TAG, "REG ID: " + mRegId);
            checkDeviceRegistration();
        }
    }

    private void checkDeliveriesStatus() {
        int completed = 0, orders = 0;//, open = 0;
        for (Marker key: mMarkersMap.keySet()) {
            Destination dest = mMarkersMap.get(key);
            // Count orders only (not restaurants)
            if (dest.mDestinationType == 0) {
                orders++;
                if (dest.mOrderStatus == ApiHelper.OrderStatus.Completed.ordinal() ||
                    dest.mOrderStatus == ApiHelper.OrderStatus.NotCompleted.ordinal())
                    completed++;
            }
        }
        if (completed == orders)//mMarkersMap.size())
            getCarrierStatusFromServer();
    }

    private void toggleMapCentering() {
        mTrackMyPosition = !mTrackMyPosition;
        final ImageButton mTrackMe = (ImageButton) findViewById(R.id.btnTrackMe);
        mTrackMe.setBackgroundResource(mTrackMyPosition ? R.drawable.rectangle_white_50 : R.drawable.rectangle_white_50_redborder);
    }

    private void updateSettingsFromServer() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 1);
        GpsAlarmReceiver.setAlarm(this, "1", cal);
        RunOn.taskThread(new Runnable() {
            @Override
            public void run() {
                getStaticValues();
            }
        }, new RunOn.TaskListener() {
            @Override
            public void onPostExecute() {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.HOUR, 1);
                GpsAlarmReceiver.setAlarm(RidersApp.getAppContext(), "1", cal);
            }
        });
    }

    private void startGpsService() {
        Intent serviceIntent = new Intent(this, RidersGpsService.class);
        serviceIntent.putExtra(Constants.TRANSMIT_FREQ, mAppSettings.mTransmitFreq);
//        if (!isMyServiceRunning())
        startService(serviceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDeliveryStatusReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSettingsUpdateReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUnsentCountReceiver);

        if (mGpsInfoTimer != null) {
            mGpsInfoTimer.cancel();
        }

        //ApiHelper.getInstance().removeListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.LOGIN_REQUEST_CODE) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            } else if (resultCode == RESULT_FIRST_USER) {
                //SuperToast.create(this, "Forgot password placeholder", SuperToast.Duration.LONG).show();
                mLoggedIn = false;
                startActivityForResult(new Intent(this, ForgotPasswordActivity.class), Constants.FORGOT_PASSWORD_REQUEST_CODE);
            } else {
                mLoggedIn = true;
                getCarrierStatusFromServer();
                RunOn.mainThreadDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDestination();
                    }
                }, 500);
            }
        } else if (requestCode == Constants.DESTINATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                /*String destinationId = data.getStringExtra("destinationId");
                for (Marker marker: mMarkersMap.keySet()) {
                    Destination dest = mMarkersMap.get(marker);
                    if (dest.mDestinationId.equalsIgnoreCase(destinationId)) {
                        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        break;
                    }
                }*/
                Log.i(TAG, "Finished DestinationActivity");
            }
        } else if (requestCode == Constants.FORGOT_PASSWORD_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                SuperToast.create(this, "Forgot password failed", SuperToast.Duration.LONG).show();
                finish();
            }
        }
    }

    private void getCarrierStatusFromServer() {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject()
                    .name("request").beginObject()
                    .name("LoadOptions").value("GetCarrierStatus")
                    .name("DataItem").beginObject()
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .endObject()
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, request.toString());
            ApiHelper.getInstance().fireSetCarrierInformationRequest(request, new ApiHelper.ApiHelperListener() {
                @Override
                public void notifyError(String error) {

                }

                @Override
                public void notifyGetStaticValuesFinished(JSONObject response) {

                }

                @Override
                public void notifyGetDestinationFinished(JSONObject response) {

                }

                @Override
                public void notifySetDestinationFinished(JSONObject response) {

                }

                @Override
                public void notifySetCarrierInformationFinished(JSONObject response) {
                    Log.wtf(TAG, "AG LOGIN Retrieved carries status:" + response);
                    JSONObject payload = response.optJSONObject("CarrierInformation");
                    if (payload == null)
                        payload = response;
                    if (payload != null) {
                        Log.wtf(TAG, "CarrierInformation: " + payload.toString());
                        int statusCode = payload.optInt("StatusCode", 0);
                        if (statusCode < ApiHelper.RiderStatus.values().length)
                            RidersGpsService.mRiderStatus = ApiHelper.RiderStatus.values()[statusCode];
                    }
                }

                @Override
                public void notifySetCarrierInformationListFinished(JSONObject response) {

                }

                @Override
                public void notifySetCustomerInformationFinished(JSONObject response) {

                }

                @Override
                public void notifyGetOrderFinished(JSONObject response) {

                }

                @Override
                public void notifyLoginOperationsFinished(JSONObject response) {

                }

                @Override
                public void notifySetDeviceInformationFinished(JSONObject response) {

                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("loggedIn", mLoggedIn);
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setMyLocationEnabled(true);

        UiSettings settings = mMap.getUiSettings();
        settings.setMyLocationButtonEnabled(true);
        settings.setScrollGesturesEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setZoomControlsEnabled(false);
        //settings.setMapToolbarEnabled(false);

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (mLocation == null) {
                    mLocation = location;
                    double lat = location.getLatitude();
                    double lon = location.getLongitude();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 12));
                    getDestination();
                }
            }
        });

//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                return false;
//            }
//        });
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Destination dest = mMarkersMap.get(marker);
                if (dest == null) {
                    Crouton.showText(MapsActivity.this, getString(R.string.err_no_info_found), Style.ALERT);
                    return;
                }
                showDestinationInfo(dest);
            }
        });

        Location location = mMap.getMyLocation();
        if (location == null)
            return;

        double lat = location.getLatitude();
        double lon = location.getLongitude();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 12));

        mLocation = location;
    }

    private void showDestinationInfo(Destination dest) {
        Intent i = new Intent(this, DestinationActivity.class);
        i.putExtra("destination", dest);
        i.putExtra("order", mOrdersMap.get(dest.getOrderId()));
        startActivityForResult(i, Constants.DESTINATION_REQUEST_CODE);
    }

    /**
     * If it's the emulator return a dummy ID, otherwise get the
     * device ID from the telephony manager
     */
    public static String getDeviceId(final Context context) {
        if (BuildConfig.DEBUG) {
            return "111111";
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD &&  Build.SERIAL.equals("unknown")) {
            return "358350040458868";
        }

        TelephonyManager telephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if(telephonyMgr != null) {
            String id = telephonyMgr.getDeviceId();
            if(id != null) {
                return id;
            }
        }

        WifiManager wMgr = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        if( wMgr == null ) {
            return NO_ID_EMPTY_STRING;
        }

        WifiInfo netInfo = wMgr.getConnectionInfo();
        if(netInfo == null ) {
            return NO_ID_EMPTY_STRING;
        }

        if(netInfo.getMacAddress() != null) {
            String mac = netInfo.getMacAddress();
            StringBuilder strippedMac = new StringBuilder(mac.length());
            for(char c : mac.toCharArray()) {
                if(Character.isLetterOrDigit(c)) {
                    strippedMac.append(c);
                }
            }
            return strippedMac.toString();
        }

        return NO_ID_EMPTY_STRING;
    }

    public void getDestination() {
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            data.put("ImeiNo", MapsActivity.getDeviceId(RidersApp.getAppContext()));
            data.put("LoadOptions","GetCarrierDestinationList");

            request.put("request", data);
            Log.d(TAG, "LOADING DESTINATIONS: " + request.toString());

            Crouton.showText(MapsActivity.this, getString(R.string.loading_destinations), Style.INFO);
            ApiHelper.getInstance().fireGetDestinationRequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getNextDestination() {
        JSONObject request = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            data.put("ImeiNo", MapsActivity.getDeviceId(RidersApp.getAppContext()));
            data.put("LoadOptions","NextCarrierDestinationList");

            request.put("request", data);
            Log.d(TAG, "LOADING DESTINATIONS: " + request.toString());

            Crouton.showText(MapsActivity.this, getString(R.string.loading_destinations), Style.INFO);
            ApiHelper.getInstance().fireGetDestinationRequest(request);
            RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Busy);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private GetDestinationResult displayDestinations(JSONObject response) {
        try {
            if (response == null)
                return null;

            Gson gson = new Gson();
            Type listType = new TypeToken<GetDestinationResult>() {}.getType();
            GetDestinationResult result = gson.fromJson(response.toString(), listType);

            LatLngBounds.Builder builder = LatLngBounds.builder();
            mMap.clear();
            mMarkersMap.clear();
            Location currLoc = mMap.getMyLocation();
            if (currLoc != null) {
                LatLng markerPoint = new LatLng(currLoc.getLatitude(), currLoc.getLongitude());
                builder.include(markerPoint);
            } else {
                Log.wtf(TAG, "NO LOCATION, SKIP SHOWING DESTINATIONS");
                return result;
            }
            if (result.mDestinations != null) {
                int cnt = 0;
                for (Destination dest : result.mDestinations) {
                    if (MapsActivity.getDeviceId(this).endsWith("1111")) {
                        if (cnt % 2 == 0) {
                            dest.mLatitude = RidersGpsService.mLAT;//52.2065;
                            dest.mLongitude = RidersGpsService.mLON;//0.1276;
                        } else {
                            dest.mLatitude = 52.2067;
                            dest.mLongitude = 0.1275;
                        }
                    }
                    cnt++;
                    //
                    LatLng markerPoint = new LatLng(dest.mLatitude, dest.mLongitude);
                    builder.include(markerPoint);
                    BitmapDescriptor icon = getMarkerIcon(dest.mOrderStatus,
                            ApiHelper.DestinationType.values()[dest.mDestinationType]);
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(markerPoint).anchor(0.50f, 0.50f).icon(icon));
                    OrderInfo orderInfo = mOrdersMap.get(dest.getOrderId());
                    if (orderInfo != null)
                        marker.setTitle(getString(R.string.customer,orderInfo.mCustomerName));
                    else
                        marker.setTitle(getString(R.string.destination_num));
                    marker.setSnippet(dest.mAddress);

                    mMarkersMap.put(marker, dest);

                    Log.wtf(TAG, "ADDED " + dest.mLatitude + ", " + dest.mLongitude);

                    /*if (!TextUtils.isEmpty(dest.mRestaurantId) &&
                        !dest.mRestaurantId.equalsIgnoreCase("00000000-0000-0000-0000-000000000000")
                        && (dest.mDestinationType >=1 && dest.mDestinationType <= 7))
                        getOrder(dest.mRestaurantId);
                    else if (!TextUtils.isEmpty(dest.mSalesOrderId) &&
                            !dest.mSalesOrderId.equalsIgnoreCase("00000000-0000-0000-0000-000000000000"))
                        getOrder(dest.mSalesOrderId);*/
                    /*****
                    if (!TextUtils.isEmpty(dest.mRestaurantId) &&
                            !dest.mRestaurantId.equalsIgnoreCase(Constants.EMPTY_GUID_STRING)
                            && dest.mDestinationType > 0)
                        getOrder(dest.mSalesOrderId, dest.mRestaurantId);
                    else if (!TextUtils.isEmpty(dest.mSalesOrderId) &&
                            !dest.mSalesOrderId.equalsIgnoreCase(Constants.EMPTY_GUID_STRING))
                        getOrder(dest.mSalesOrderId, Constants.EMPTY_GUID_STRING);
                    else
                        getOrder(Constants.EMPTY_GUID_STRING, Constants.EMPTY_GUID_STRING);
                     */
                    getOrder(dest.mSalesOrderId, dest.mRestaurantId);
                }

                if (result.mDestinations.size() > 0) {
                    DisplayMetrics dm = getResources().getDisplayMetrics();
                    int padding = (int) (90 * dm.scaledDensity);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),
                            dm.widthPixels - padding, dm.heightPixels - padding, 60));
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getOrder(String orderId, String restaurantId) {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject();
            jsonWriter.name("request").beginObject()
                    .name("LoadOptions").value("GetOrderInfo")
                    .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                    .name("OrderId").value(orderId)
                    .name("RestaurantId").value(restaurantId)
                    .endObject()
                    .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());

            Log.d(TAG, "GETTING ORDER: " + request.toString());
            ApiHelper.getInstance().fireGetOrderRequest(request);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public void getStaticValues() {
        try {
            final JSONObject request = new JSONObject();
            JSONObject data = new JSONObject();
            data.put("ImeiNo", MapsActivity.getDeviceId(RidersApp.getAppContext()));
            request.put("request", data);
            Log.d(TAG, "GETTING SETTINGS: " + request.toString());

            ApiHelper.getInstance().fireGetStaticValuesRequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveAndApplySettings(JSONObject payload) {
        Gson gson = new Gson();
        Type responseType = new TypeToken<GetStaticValuesResult>() {}.getType();
        GetStaticValuesResult result = gson.fromJson(payload.toString(), responseType);
        Log.wtf(TAG, result.toString());
        if (result.mResult != 1) {
            Log.wtf(TAG, "GetStaticValues.Result = " + result.mResult + " - Skipping...");
            return;
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit()
                .putInt(Constants.CUSTOMER_RANGE, result.mValues.mCustomerRange)
                .putInt(Constants.HOME_RANGE, result.mValues.mHomeRange)
                .putInt(Constants.STATIONARY_MIN, result.mValues.mStationaryMin)
                .putInt(Constants.STATIONARY_RANGE, result.mValues.mStationaryRange)
                .putInt(Constants.TRANSMIT_FREQ, result.mValues.mTransmitFreq)
        .apply();
        applySettings(sp, true);
    }

    private void applySettings(boolean updateService) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        applySettings(sp, updateService);
    }

    private void applySettings(SharedPreferences sp, boolean updateService) {
        mAppSettings.mCustomerRange = sp.getInt(Constants.CUSTOMER_RANGE, 0);
        mAppSettings.mHomeRange = sp.getInt(Constants.HOME_RANGE, 0);
        mAppSettings.mStationaryMin = sp.getInt(Constants.STATIONARY_MIN, 0);
        mAppSettings.mStationaryRange = sp.getInt(Constants.STATIONARY_RANGE, 0);
        mAppSettings.mTransmitFreq = sp.getInt(Constants.TRANSMIT_FREQ, 10);
        if (updateService) {
            startGpsService();
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager == null)
            return false;
        List<ActivityManager.RunningServiceInfo> services = manager.getRunningServices(Integer.MAX_VALUE);
        if (services == null)
            return false;
        for (ActivityManager.RunningServiceInfo service : services) {
            if (RidersGpsService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /** CENTER MAP TO SEE MY LOCATION ALL THE TIME */
    public void centerMap() {
        if (mMap == null)
            return;
        Location loc = mMap.getMyLocation();
        if (loc == null)
            return;
        centerMap(loc);
    }

    public void centerMap(Location loc) {
        double lat = loc.getLatitude();
        double lon = loc.getLongitude();
        centerMap(lat, lon);
    }

    public void centerMap(double lat, double lon) {
        if (!mTrackMyPosition)
            return;
        Log.v(TAG, String.format("Centering map to [%f,%f]", lat, lon));

        Projection projection = this.mMap.getProjection();
        LatLngBounds bounds = projection.getVisibleRegion().latLngBounds;
        LatLng currentPoint = new LatLng(lat, lon);
        if(bounds.contains(currentPoint))
            return;

        LatLngBounds.Builder builder = LatLngBounds.builder();
        //mMap.clear();
        ///mMarkersMap.clear();
        LatLng currMarkerPoint = new LatLng(lat, lon);
        builder.include(currMarkerPoint);
        for (Marker marker: mMarkersMap.keySet()) {
            Destination dest = mMarkersMap.get(marker);
            LatLng markerPoint = new LatLng(dest.mLatitude, dest.mLongitude);
            builder.include(markerPoint);
        }

        mMap.stopAnimation();

        DisplayMetrics dm = getResources().getDisplayMetrics();
        int padding = (int)(90 * dm.scaledDensity);
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),
                dm.widthPixels - padding, dm.heightPixels - padding, 60));
    }

    BroadcastReceiver mUnsentCountReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mUnsentInfo.setText(getString(R.string.unsent, intent.getExtras().getInt("count", 0)));
        }
    };

    BroadcastReceiver mSettingsUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    BroadcastReceiver mDeliveryStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String destinationId = intent.getExtras().getString("destinationId", "0");
            int status = intent.getExtras().getInt("status", 0);
            for (Marker marker: mMarkersMap.keySet()) {
                Destination dest = mMarkersMap.get(marker);
                if (dest.mDestinationId.equalsIgnoreCase(destinationId)) {
                    dest.mOrderStatus = status;
                    BitmapDescriptor icon = getMarkerIcon(status,
                            ApiHelper.DestinationType.values()[dest.mDestinationType]);
                    marker.setIcon(icon);
                    break;
                }
            }
            checkDeliveriesStatus();
        }
    };

    private BitmapDescriptor getMarkerIcon(int status, ApiHelper.DestinationType destinationType) {
        BitmapDescriptor icon;
        switch (destinationType) {
            default:
                switch (ApiHelper.OrderStatus.values()[status]) {
                    case Waiting:
                    case Assign:
                        icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
                        break;
                    case Completed:
                        icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
                        break;
                    default:
                        icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                        break;
                }
                break;
            case BurgerKıng:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.bk);
                break;
            case Popoyes:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.pop);
                break;
            case Sbarro:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.sbarro);
                break;
            case Arbys:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.arbys);
                break;
            case UstaDonerci:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.usta);
                break;
            case BurgerCity:
                icon = BitmapDescriptorFactory.fromResource(R.drawable.city);
                break;
        }
        return icon;
    }

    @Override
    public void notifyError(String error) {
        Crouton.showText(this, error, Style.ALERT);
//        if (!TextUtils.isEmpty(MapsActivity.mLastLoadOptions) &&
//            MapsActivity.mLastLoadOptions.equalsIgnoreCase("SetGcmRegistrationId")) {
//            MapsActivity.mLastLoadOptions = "";
//        }
    }

    @Override
    public void notifyGetStaticValuesFinished(JSONObject response) {
        JSONObject payload = response.optJSONObject("GetStaticValuesResult");
        if (payload == null)
            payload = response;
        if (payload != null) {
            Log.wtf(TAG, "GetStaticValuesResult: " + payload.toString());
            Log.wtf(TAG, "Processing GetStaticValuesResult...");
            saveAndApplySettings(payload);
        }
    }

    @Override
    public void notifyGetDestinationFinished(JSONObject response) {
        JSONObject payload = response.optJSONObject("GetDestinationResult");
        if (payload == null)
            payload = response;
        if (payload != null) {
            Log.wtf(TAG, "GetDestinationResult: " + payload.toString());
            Log.wtf(TAG, "Processing GetDestinationResult...");
            GetDestinationResult result = displayDestinations(response);

            //RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Busy);

            if (result != null && result.mDestinations != null && result.mDestinations.size() > 0) {
                Intent intent = new Intent(Constants.ACTION_GEO_CREATE);
                intent.putExtra("destinations", result.mDestinations);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            } else {
                RidersGpsService.reportDriverStatus(ApiHelper.RiderStatus.Available);
            }
        }
    }

    @Override
    public void notifyGetOrderFinished(JSONObject response) {
        Log.wtf(TAG, "ORDER INFO: " + response.toString());
        try {
            JSONObject info = response.getJSONObject("OrderInfo");

            Gson gson = new Gson();
            Type dataType = new TypeToken<OrderInfo>() {}.getType();
            OrderInfo orderInfo = gson.fromJson(info.toString(), dataType);
            mOrdersMap.put(orderInfo.mOrderId, orderInfo);

            Set<Marker> keys = mMarkersMap.keySet();
            for (Marker marker: keys) {
                if (mMarkersMap.get(marker).getOrderId().equalsIgnoreCase(orderInfo.mOrderId)) {
                    marker.setTitle(orderInfo.mCustomerName);
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void notifySetDeviceInformationFinished(JSONObject response) {
        final SharedPreferences prefs = getGCMPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        Log.i(TAG, "REGISTERED");
        editor.putBoolean(PROPERTY_REGISTERED_WITH_WS, true);
        editor.apply();
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
//        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
//                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
//        }
        return ConnectionResult.SUCCESS == resultCode;
    }

    private String getNameFromType(int activityType) {
        switch(activityType) {
            case DetectedActivity.IN_VEHICLE:
                return getString(R.string.in_vehicle);
            case DetectedActivity.ON_BICYCLE:
                return getString(R.string.on_bicycle);
            case DetectedActivity.ON_FOOT:
                return getString(R.string.on_foot);
            case DetectedActivity.STILL:
                return getString(R.string.still);
            case DetectedActivity.UNKNOWN:
                return getString(R.string.unknown);
            case DetectedActivity.TILTING:
                return getString(R.string.tilting);
            case DetectedActivity.WALKING:
                return getString(R.string.walking);
            case DetectedActivity.RUNNING:
                return getString(R.string.running);
        }
        return getString(R.string.unknown);
    }

    /**
     * GCM
     */
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_REGISTERED_WITH_WS = "registered_with_tn";
    public static final String GCM_PREFERENCES = "riders_gcm";

    /**
     * This is the project number you got from the API Console, as described in "Getting Started."
     */
    private static final String SENDER_ID = "632529666739";


    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(GCM_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (mGCM == null) {
                        mGCM = GoogleCloudMessaging.getInstance(RidersApp.getAppContext());
                    }
                    Log.i(TAG, "registerInBackground() => GOT past mGCM creation");
                    Log.i(TAG, "registerInBackground() => ABOUT TO REGISTER for PROJECT ID = " + SENDER_ID);
                    mRegId = mGCM.register(SENDER_ID);
                    Log.i(TAG, "registerInBackground() => GOT past register()");
                    msg = "registerInBackground() => Device registered, registration ID = " + mRegId;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(RidersApp.getAppContext(), mRegId);
                } catch (IOException | SecurityException ex) {
                    msg = "registerInBackground() => Error: " + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG, msg);
            }

            /**
             * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
             * or CCS to send messages to your app. Not needed for this demo since the
             * device sends upstream messages to a server that echoes back the message
             * using the 'from' address in the message.
             */
            private void sendRegistrationIdToBackend() {
                Log.i(TAG, "ABOUT TO SEND DATA TO WEBSERVICE");
                enqueueRegisterDeviceRequest();
            }
        }.execute();
    }

    /**
     * Check if we already registered with webservice
     */
    private void checkDeviceRegistration() {
        final SharedPreferences prefs = getGCMPreferences(this);
        boolean success = prefs.getBoolean(PROPERTY_REGISTERED_WITH_WS, false);
        if (success) {
            Log.i(TAG, "ALREADY REGISTERED. SKIP DEVICE REGISTRATION");
            return;
        }
        enqueueRegisterDeviceRequest();
    }

    /**
     * Send registration into to webservice
     */
    private void enqueueRegisterDeviceRequest() {
        // TODO: after getting the response, save 'registered' flag to SharedPreferences
//        boolean success = true;
//        final SharedPreferences prefs = getGCMPreferences(this);
//        SharedPreferences.Editor editor = prefs.edit();
//        if (success) {
//            Log.i(TAG, "REGISTERED");
//            editor.putBoolean(PROPERTY_REGISTERED_WITH_WS, true);
//        } else {
//            Log.i(TAG, "NOT REGISTERED");
//            editor.putBoolean(PROPERTY_REGISTERED_WITH_WS, false);
//        }
//        editor.apply();
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter
                .beginObject();
                    jsonWriter.name("request")
                    .beginObject()
                        .name("LoadOptions").value("SetGcmRegistrationId")
                        .name("DataItem")
                            .beginObject()
                                .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                                .name("GcmRegistrationId").value(mRegId)
                            .endObject()
                    .endObject()
                .endObject();

            JSONObject request = new JSONObject(stringWriter.toString());
            Log.d(TAG, request.toString());

            //MapsActivity.mLastLoadOptions = "SetGcmRegistrationId";
            ApiHelper.getInstance().fireSetDeviceInformationRequest(request);
        } catch (JSONException | IOException e) {
            //MapsActivity.mLastLoadOptions = "";
            e.printStackTrace();
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();

        RidersGpsService.addLog(this, "riders_gcm.txt", regId);
    }

    private void sendGCMMessage(final String message) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    Bundle data = new Bundle();
                    data.putString("my_message", message);
                    data.putString("my_action", "com.touchnote.android.UPSTREAM_MSG");
                    String id = Integer.toString(mMsgId.incrementAndGet());
                    mGCM.send(SENDER_ID + "@gcm.googleapis.com", id, data);
                    msg = "Sent message";
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG, msg);
            }
        }.execute(null, null, null);
    }
}
