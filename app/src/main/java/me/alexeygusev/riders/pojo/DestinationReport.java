package me.alexeygusev.riders.pojo;

import java.io.Serializable;

import me.alexeygusev.riders.ws.ApiHelper;

/**
 * Created by Alex on 10/12/14.
 */
public class DestinationReport implements Serializable {
    public String mAddressId;
    public String mDestinationId;
    public String mOrderId;
    public ApiHelper.OrderStatus mStatus;
    public long mTimestamp;
    public boolean mSent;
}
