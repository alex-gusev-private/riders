package me.alexeygusev.riders.pojo;

import android.content.Intent;

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by Alex on 30/11/14.
 */
public class StaticValues {
    @SerializedName("customerRange")
    public Integer mCustomerRange;
    @SerializedName("homeRange")
    public Integer mHomeRange;
    @SerializedName("stationaryMin")
    public Integer mStationaryMin;
    @SerializedName("stationaryRange")
    public Integer mStationaryRange;
    @SerializedName("transmitFreq")
    public Integer mTransmitFreq;

    public String toString() {
        return String.format(Locale.ENGLISH, "CustomerRange: %d, HomeRange: %d, StationaryMin: %d, StationaryRange: %d, TransmitFreq: %d",
                mCustomerRange, mHomeRange, mStationaryMin, mStationaryRange, mTransmitFreq);
    }
}
