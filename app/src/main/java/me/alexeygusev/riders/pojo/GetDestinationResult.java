package me.alexeygusev.riders.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Alex on 13/11/14.
 */
public class GetDestinationResult {
    @SerializedName("ErrorCode")
    public String mErrorCode;
    @SerializedName("Message")
    public String mMessage;
    @SerializedName("Result")
    public Integer mResult;
    @SerializedName("Destinations")
    public ArrayList<Destination> mDestinations;

}
