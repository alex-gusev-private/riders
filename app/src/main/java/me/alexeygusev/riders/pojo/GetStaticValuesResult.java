package me.alexeygusev.riders.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Alex on 30/11/14.
 */
public class GetStaticValuesResult {
    @SerializedName("ErrorCode")
    public String mErrorCode;
    @SerializedName("Message")
    public String mMessage;
    @SerializedName("Result")
    public Integer mResult;
    @SerializedName("StaticValues")
    public StaticValues mValues;

    public String toString() {
        return String.format(Locale.ENGLISH, "ErrorCode: %s, Message: %s, Result: %d, StaticValues: [%s]",
                mErrorCode, mMessage, mResult, mValues.toString());
    }
}
