package me.alexeygusev.riders.pojo;

import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import me.alexeygusev.riders.MapsActivity;
import me.alexeygusev.riders.RidersApp;
import me.alexeygusev.riders.ws.ApiHelper;

/**
 * Created by Alex on 21/11/14.
 */
public class GpsReport implements Serializable {
    private final static String TAG = "RIDERS-GPSREPORT";

    public int mID;
    public double mLat;
    public double mLon;
    public double mAccuracy;
    public boolean mGps;
    public long mTimestamp;
    public boolean mSent;

    public interface Callback {
        void onSuccess(int jobId);
        void onFailure();
    }
}
