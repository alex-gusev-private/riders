package me.alexeygusev.riders.pojo;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import me.alexeygusev.riders.Constants;

/**
 * Created by Alex on 13/11/14.
 */
public class Destination implements Serializable {
    @SerializedName("Address")
    public String mAddress;
    @SerializedName("AddressId")
    public String mAddressId;
    @SerializedName("DestinationId")
    public String mDestinationId;
    @SerializedName("DestinationType")
    public Integer mDestinationType;
    @SerializedName("ImeiNo")
    public String mImeiNo;
    @SerializedName("Latitude")
    public Double mLatitude;
    @SerializedName("Longitude")
    public Double mLongitude;
    @SerializedName("Message")
    public String mMessage;
    @SerializedName("Order")
    public int mOrder;
//    @SerializedName("OrderId")
//    public String mOrderId;
    @SerializedName("OrderStatus")
    public int mOrderStatus;
    @SerializedName("Phone")
    public String mPhone;
    @SerializedName("Range")
    public int mRange;
    @SerializedName("IsactiveList")
    public boolean mIsactiveList;
    @SerializedName("OrderPackageNumber")
    public int mOrderPackageNumber;
    @SerializedName("RestaurantId")
    public String mRestaurantId;
    @SerializedName("DestinationPriority")
    public int mDestinationPriority;
    @SerializedName("SalesOrderId")
    public String mSalesOrderId;

    public String getOrderId() {
        if (!TextUtils.isEmpty(mRestaurantId) &&
                !mRestaurantId.equalsIgnoreCase(Constants.EMPTY_GUID_STRING)
                && (mDestinationType > 0))
            return mRestaurantId;
        else if (!TextUtils.isEmpty(mSalesOrderId) &&
                !mSalesOrderId.equalsIgnoreCase(Constants.EMPTY_GUID_STRING))
            return mSalesOrderId;
        else
            return "";
    }
}
