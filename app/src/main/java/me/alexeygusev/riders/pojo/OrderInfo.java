package me.alexeygusev.riders.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alex on 16/12/14.
 */
public class OrderInfo  implements Serializable {
    @SerializedName("Address")
    public String mAddress;
    @SerializedName("DeliveryOrder")
    public int mDeliveryOrder;
    @SerializedName("Latitude")
    public Double mLatitude;
    @SerializedName("Longitude")
    public Double mLongitude;
    @SerializedName("OrderId")
    public String mOrderId;
    @SerializedName("OrderStatus")
    public int mOrderStatus;
    @SerializedName("AddressId")
    public String mAddressId;
    @SerializedName("CustomerName")
    public String mCustomerName;
    @SerializedName("CustomerNote")
    public String mCustomerNote;
    @SerializedName("CustomerOrderInfo")
    public String mCustomerOrderInfo;
    @SerializedName("ReceiptNumber")
    public int mReceiptNumber;
}
//{
//    "AddressId":"17ab0ccf-5b1b-e411-9402-005056800c1e",
//    "OrderStatus":0,
//    "CustomerOrderInfo":" Texassmokehouse™ Burger Menü x 2.0000000000",
//    "DeliveryOrder":0,
//    "OrderId":"8694ba6a-ee9b-e411-940b-005056801a6f",
//    "Address":"Akmerkezden gelirken ışıklardan ilk sağa",
//    "ReceiptNumber":8,
//    "Latitude":0,
//    "Longitude":0,
//    "CustomerNote":"  Servis (plastik çatal, peçete vs.) İSTEMİYORUM.   TEL :   2123246789 ",
//    "CustomerName":"BERTAN AKARSU"
//}
