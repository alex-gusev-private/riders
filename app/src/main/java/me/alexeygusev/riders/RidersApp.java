package me.alexeygusev.riders;

/**
 * Created by Alex Gusev on 30/04/2014.
 * Project: Fidel.
 *
 * Copyright (c) 2014. All rights reserved.
 */

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import com.bugsnag.android.Bugsnag;

public class RidersApp extends Application {

    public static final String BUGSNAG_APP_ID = "48a9a965a37eec473b7fea03dbbe3abf";

    private static Context sContext = null;

    @Override
    public void onCreate() {

        RidersApp.sContext = getApplicationContext();

        /**
         * AG: report the crashes always.
         * Comment out "if" line when you don't want to receive debugging reports.
         */
        if (!BuildConfig.DEBUG) {
            Bugsnag.register(this, BUGSNAG_APP_ID);
        }


        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.LAX);
        StrictMode.setVmPolicy(StrictMode.VmPolicy.LAX);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int language = sp.getInt("language", 0);
        switch (language) {
            default:
                CustomLanguage.setLanguage(this, "tr", true);
                break;
            case 2:
                CustomLanguage.setLanguage(this, "zh", true);
                break;
        }
        //CustomLanguage.setLanguage(this, "tr", true);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static Context getAppContext() {
        return RidersApp.sContext;
    }
}
