package me.alexeygusev.riders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.util.JsonWriter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringWriter;

import me.alexeygusev.riders.runnable.RunOn;
import me.alexeygusev.riders.ws.ApiHelper;

public class LoginActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = "RIDERS-LOGIN";

	Button btnOK;
	Button btnCancel;
    Button btnForgotPassword;
	EditText etPIN;
	EditText etJID;
	String pwd;

	int mVersionCode = 0;

    ProgressDialog mProgress;

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.login);

		TextView version = (TextView) findViewById(R.id.textViewVersion);
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo("me.alexeygusev.riders", 0);
			version.setText(getString(R.string.app_name) + " v" + info.versionName);
			pwd = MapsActivity.getDeviceId(this);
			pwd = pwd.substring(pwd.length() - 4);
		} catch (NameNotFoundException e) {
			
			e.printStackTrace();
		}
		if (info != null)
			mVersionCode = info.versionCode;

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int language = sp.getInt("language", 0);
        boolean isChecked = language < 2;
        Log.wtf("LANG", "[0] LANG = " + language);

        final Switch langSwitch = (Switch) findViewById(R.id.switch1);
        langSwitch.setChecked(isChecked);
        langSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int language;
                if (isChecked)
                    language = 1;
                else
                    language = 2;
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                sp.edit().putInt("language", language).apply();

                Log.wtf("LANG", "LANG = " + language);
                setLanguage(language, true);
            }
        });

        etPIN = (EditText) findViewById(R.id.editTextPin);
        etJID = (EditText) findViewById(R.id.editTextMgr);

		boolean bypass = getIntent().getBooleanExtra("bypass", false);

		if (!bypass) {
//			VersionUpdater versionUpdater = new VersionUpdater(this, null);
//			versionUpdater.execute();
		} else {
			// TODO: bypass login
            etPIN.setText(pwd);
            RunOn.mainThreadDelayed(new Runnable() {
                @Override
                public void run() {
                    handleOkClick();
                }
            }, 250);
		}

        etPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                handleOkClick();
                return true;
            }
        });
        etJID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                handleOkClick();
                return true;
            }
        });
        etJID.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        btnOK = (Button) findViewById(R.id.buttonOK);
		btnOK.setOnClickListener(this);
		
		btnCancel = (Button) findViewById(R.id.buttonCancel);
		btnCancel.setOnClickListener(this);

        btnForgotPassword = (Button) findViewById(R.id.buttonForgotPassword);
        btnForgotPassword.setOnClickListener(this);

		Log.i(TAG, this.getClass().getSimpleName() + ".onCreate()");

        etPIN.requestFocus();
	}

    private void setLanguage(int language, boolean recreate) {
        switch (language) {
            case 0:
                CustomLanguage.setLanguage(getApplication(), "en", true);
                break;
            case 1:
                CustomLanguage.setLanguage(getApplication(), "tr", true);
                break;
            case 2:
                CustomLanguage.setLanguage(getApplication(), "zh", true);
                break;
        }
        if (recreate) {
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

	@Override
	public void onClick(View v) {
		if (v == btnOK) {
            handleOkClick();
		} else if (v == btnCancel) {
			setResult(RESULT_CANCELED);
			finish();
		} else if (v == btnForgotPassword) {
            setResult(RESULT_FIRST_USER);
            finish();
        }
	}

    private void handleOkClick() {
        String pin = etPIN.getEditableText().toString();
        if (TextUtils.isEmpty(pin) || !pin.equals(pwd)) {
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.incorrect_pin));
            superToast.show();
            etPIN.setText("");
            etPIN.requestFocus();
            return;
        }
        String phone = etJID.getEditableText().toString();
        if (TextUtils.isEmpty(phone)) {
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.enter_telephone));
            superToast.show();
            etJID.requestFocus();
            return;
        }

        try {
            ApiHelper.getInstance().addListener(this);

            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.beginObject()
                .name("request").beginObject()
                    .name("LoadOptions").value("Login")
                    .name("DataItem").beginObject()
                        .name("ImeiNo").value(MapsActivity.getDeviceId(RidersApp.getAppContext()))
                        .name("Password").value(pin)
                        .name("PhoneNumber").value(phone)
                    .endObject()
                .endObject()
            .endObject();

            mProgress = new ProgressDialog(this);
            mProgress.setMessage(getString(R.string.logging_in));
            mProgress.setTitle(R.string.app_name);
            mProgress.setIcon(R.drawable.ic_launcher);
            mProgress.show();

            JSONObject request = new JSONObject(stringWriter.toString());
            ApiHelper.getInstance().fireLoginOperationsRequest(request);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            ApiHelper.getInstance().removeListener(this);
            if (mProgress != null && mProgress.isShowing()) {
                mProgress.dismiss();
                mProgress = null;
            }
        }

    }

    @Override
    public void notifyError(String error) {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }
        super.notifyError(error);
    }

    @Override
    public void notifyLoginOperationsFinished(JSONObject response) {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }

        Log.wtf("LOGIN", response.toString());
        try {
            JSONObject obj = new JSONObject(response.toString());
            String errorCode = obj.getString("ErrorCode");
            if (TextUtils.isEmpty(errorCode) || errorCode.equalsIgnoreCase("null")) {
                setResult(RESULT_OK);
                finish();
            } else {
                SuperToast superToast = new SuperToast(this);
                superToast.setAnimations(SuperToast.Animations.FADE);
                superToast.setDuration(SuperToast.Duration.LONG);
                superToast.setBackground(SuperToast.Background.RED);
                superToast.setTextSize(SuperToast.TextSize.LARGE);
                superToast.setText(getString(R.string.request_failed_with_error));
                superToast.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            SuperToast superToast = new SuperToast(this);
            superToast.setAnimations(SuperToast.Animations.FADE);
            superToast.setDuration(SuperToast.Duration.LONG);
            superToast.setBackground(SuperToast.Background.RED);
            superToast.setTextSize(SuperToast.TextSize.LARGE);
            superToast.setText(getString(R.string.request_failed_with_error));
            superToast.show();
        }
    }
}
