package me.alexeygusev.riders;

import com.android.volley.toolbox.StringRequest;

/**
 * Created by Alex Gusev on 02/04/2014.
 * Project: edible_android.
 * Copyright (c) 2014. All rights reserved.
 */

public class Constants {
    public static final String URL_BASE_HOST = "http://212.109.96.143:84/TFIActionService/AndroidService.svc/";
    // Settings
    public static final String METHOD_GET_STATIC_VALUES = "GetStaticValues";
    // Data
    public static final String METHOD_GET_DESTINATION = "GetDestination";
    public static final String METHOD_SET_DESTINATION = "SetDestination";
    public static final String METHOD_SET_CARRIER_INFORMATION = "SetCarrierInformation";
    public static final String METHOD_SET_CUSTOMER_INFORMATION = "SetCustomerInformation";
    public static final String METHOD_GET_ORDER = "GetOrder";
    public static final String METHOD_LOGIN_OPERATIONS = "LoginOperations";
    public static final String METHOD_SET_DEVICE_INFORMATION = "SetDeviceInformation";

    // Unused
    public static final String METHOD_SET_CARRIER_INFORMATION_LIST = "SetCarrierInformationList";

    public static final String CUSTOMER_RANGE = "customerRange";
    public static final String HOME_RANGE = "homeRange";
    public static final String STATIONARY_MIN = "stationaryMin";
    public static final String STATIONARY_RANGE = "stationaryRange";
    public static final String TRANSMIT_FREQ = "transmitFreq";

    public static final int LOGIN_REQUEST_CODE = 0x1000;
    public static final int DESTINATION_REQUEST_CODE = 0x1001;
    public static final int FORGOT_PASSWORD_REQUEST_CODE = 0x1002;
    public static final String ACTION_DELIVERY_STATUS = "me.alexeygusev.riders.DELIVERY_STATUS";
    public static final String ACTION_UNSENT_COUNT = "me.alexeygusev.riders.UNSENT_COUNT";

    public static final String ACTION_RIDER_ACTIVITY = "me.alexeygusev.riders.RIDER_ACTIVITY";
    public static final String ACTION_GEO_CREATE = "me.alexeygusev.riders.GEO_CREATE";
    public static final String ACTION_GEO_TRANSITION = "me.alexeygusev.riders.GEO_TRANSITION";

    /**
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public static final String EMPTY_GUID_STRING = "00000000-0000-0000-0000-000000000000";
}
