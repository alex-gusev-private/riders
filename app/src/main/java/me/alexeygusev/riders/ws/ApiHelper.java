package me.alexeygusev.riders.ws;

import android.net.http.AndroidHttpClient;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

import me.alexeygusev.riders.Constants;
import me.alexeygusev.riders.RidersApp;

/**
 * Created by Alex Gusev on 17/10/2014.
 * Project: Riders.
 * Copyright (c) 2014. All rights reserved.
 */
public class ApiHelper {

    public static final String TAG = "RIDERS-API-HELPER";

    public static RequestQueue sRequestQueue;
    public static RequestQueue sPositionRequestQueue;

    private static ApiHelper sThis = null;

    public interface ApiHelperListener {
        void notifyError(String error);
        void notifyGetStaticValuesFinished(JSONObject response);
        void notifyGetDestinationFinished(JSONObject response);
        void notifySetDestinationFinished(JSONObject response);
        void notifySetCarrierInformationFinished(JSONObject response);
        void notifySetCarrierInformationListFinished(JSONObject response);
        void notifySetCustomerInformationFinished(JSONObject response);
        void notifyGetOrderFinished(JSONObject response);
        void notifyLoginOperationsFinished(JSONObject response);
        void notifySetDeviceInformationFinished(JSONObject response);
    }

    public enum RiderStatus {
        Available, Busy, Resting, Accident, BrokeDown, WaitingCustomer, Parking, GoBack
    }

    public enum OrderStatus {
        Waiting, Completed, NotCompleted, Assign
    }

    public enum DestinationType {
        Customer, BurgerKıng, Popoyes, Sbarro, Arbys, UstaDonerci, BurgerCity
    }

    //public ApiHelperListener mListener;
    public ArrayList<ApiHelperListener> mListeners = new ArrayList<>();

    public static ApiHelper getInstance() {
        if (sThis == null) {
            sThis = new ApiHelper();
            ApiHelper.sRequestQueue = Volley.newRequestQueue(RidersApp.getAppContext());
            ApiHelper.sPositionRequestQueue = Volley.newRequestQueue(RidersApp.getAppContext(),
                    new HttpClientStack(AndroidHttpClient.newInstance("volley/gps")));
        }
        return sThis;
    }

    public static Class getClazz(Object o) {
        return ((Object)o).getClass();
    }

    public void addListener(ApiHelperListener listener) {
        mListeners.add(listener);
    }

    public void removeListener(ApiHelperListener listener) {
        mListeners.remove(listener);
    }

    Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            String errorMsg = error instanceof TimeoutError ? "No network" : error.getMessage();
            if (TextUtils.isEmpty(errorMsg))
                errorMsg = error.toString();
            Log.wtf(ApiHelper.getClazz(this).getSimpleName(), errorMsg);
            if (mListeners != null) {
                for (ApiHelperListener listener : mListeners)
                    listener.notifyError("ERROR: " + errorMsg);
            }
        }
    };

    public void fireGetStaticValuesRequest(JSONObject jsonObject/*, ApiHelperListener listener*/) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_GET_STATIC_VALUES, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifyGetStaticValuesFinished(response);
                    }
                },
                mErrorListener
        ) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireGetDestinationRequest(JSONObject jsonObject/*, ApiHelperListener listener*/) {
        //mListener = listener;
        final JsonObjectWithHeadersRequest request = new JsonObjectWithHeadersRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_GET_DESTINATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifyGetDestinationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetDestinationRequest(JSONObject jsonObject/*, ApiHelperListener listener*/) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_DESTINATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifySetDestinationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetCarrierInformationRequest(JSONObject jsonObject, final ApiHelperListener listener) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_CARRIER_INFORMATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (listener != null)
                            listener.notifySetCarrierInformationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetCarrierInformationRequest(JSONObject jsonObject/*, ApiHelperListener listener*/) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_CARRIER_INFORMATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifySetCarrierInformationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetCustomerInformationRequest(JSONObject jsonObject) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_CUSTOMER_INFORMATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifySetCustomerInformationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireGetOrderRequest(JSONObject jsonObject) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_GET_ORDER, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifyGetOrderFinished(response);
                    }
                },
                mErrorListener
        ) {
            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetCarrierInformationListRequest(JSONObject jsonObject/*, ApiHelperListener listener*/) {
        //mListener = listener;
        final JsonObjectRequest request = new JsonObjectRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_CARRIER_INFORMATION_LIST, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifySetCarrierInformationListFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireLoginOperationsRequest(JSONObject jsonObject) {
        final JsonObjectWithHeadersRequest request = new JsonObjectWithHeadersRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_LOGIN_OPERATIONS, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifyLoginOperationsFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }

    public void fireSetDeviceInformationRequest(JSONObject jsonObject) {
        final JsonObjectWithHeadersRequest request = new JsonObjectWithHeadersRequest(
                Constants.URL_BASE_HOST + Constants.METHOD_SET_DEVICE_INFORMATION, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListeners != null)
                            for (ApiHelperListener listener: mListeners)
                                listener.notifySetDeviceInformationFinished(response);
                    }
                },
                mErrorListener
        );
        request.setShouldCache(false);
        ApiHelper.sRequestQueue.add(request);
    }
}
