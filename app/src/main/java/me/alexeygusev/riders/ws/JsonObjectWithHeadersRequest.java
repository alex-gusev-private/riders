package me.alexeygusev.riders.ws;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex Gusev on 22/04/2014.
 * Project: edible_android.
 * Copyright (c) 2014. All rights reserved.
 */
public class JsonObjectWithHeadersRequest extends JsonObjectRequest {

    public JsonObjectWithHeadersRequest(int method, String url, JSONObject jsonRequest,
                                        Response.Listener<JSONObject> listener,
                                        Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public JsonObjectWithHeadersRequest(String url, JSONObject jsonRequest,
                                        Response.Listener<JSONObject> listener,
                                        Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
    }
    /**
     *         // AG: Optionally, you can just use the default CookieManager
     CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
     CookieHandler.setDefault(manager);

     */
//    @Override
//    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//        // since we don't know which of the two underlying network vehicles
//        // will Volley use, we have to handle and store session cookies manually
//        if (mCookiesHelper != null)
//            mCookiesHelper.saveCookies(response.headers);
//
//        return super.parseNetworkResponse(response);
//    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null || headers.equals(Collections.emptyMap()))
            headers = new HashMap<>();

        headers.put("Accept", "application/json");

        return headers;
    }
}
